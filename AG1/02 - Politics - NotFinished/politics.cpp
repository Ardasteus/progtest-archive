#include <iostream>
#include <memory>
#include <math.h>

using namespace std;

template <typename T>
class BinomialNode
{
public:
    BinomialNode(T _data)
    {
        data = _data;
        degree = 0;
        parent = nullptr;
        child = nullptr;
        next = nullptr;
        previous = nullptr;
    }

    void dispose()
    {
        parent = nullptr;
        child = nullptr;
        next = nullptr;
        previous = nullptr;
    }

    T data;
    uint32_t degree;
    shared_ptr<BinomialNode> parent;
    shared_ptr<BinomialNode> child;
    shared_ptr<BinomialNode> next;
    shared_ptr<BinomialNode> previous;
};

template <typename T, typename Cmp = less<T>>
class BinomialHeap
{
private:
    void make_siblings(shared_ptr<BinomialNode<T>> left, shared_ptr<BinomialNode<T>> right)
    {
        if(left->next != nullptr)
            left->next->previous = nullptr;
        left->next = right;

        if(right->previous != nullptr)
            right->previous->next = nullptr;
        right->previous = left;
    }

    void make_parent(shared_ptr<BinomialNode<T>>& left, shared_ptr<BinomialNode<T>>& right)
    {
        if(right->parent != nullptr)
        {
            right->parent->child = right->next;
            if(right->next != nullptr)
                right->next->previous = nullptr;
        }

        if(right->next != nullptr && right->previous != nullptr)
            make_siblings(right->previous, right->next);
        else if(right->next != nullptr)
            right->next->previous = nullptr;
        else if(right->previous != nullptr)
            right->previous->next = nullptr;

        right->previous = nullptr;
        right->next = nullptr;

        if(left->child != nullptr)
            make_siblings(right, left->child);
        
        left->child = right;
        left->child->parent = left;
        left->degree++;
    }

public:
    shared_ptr<BinomialNode<T>> root;
    shared_ptr<BinomialNode<T>> to_extract;
    Cmp comparator;
    T extract_value;

    BinomialHeap(T extract)
    {
        root = nullptr;
        extract_value = extract;
    }

    BinomialHeap(const BinomialHeap<T, Cmp>& other)
    {
        root = other.root;
        extract_value = other.extract_value;
        to_extract = other.to_extract;
    }

    BinomialHeap<T, Cmp>& operator=(const BinomialHeap<T, Cmp>& other)
    {
        root = other.root;
        extract_value = other.extract_value;
        to_extract = other.to_extract;
        return *this;
    }

    shared_ptr<BinomialNode<T>> merge_heap(BinomialHeap<T, Cmp> other)
    {
        if(root == nullptr)
        {
            root = other.root;
            return nullptr;
        }
        shared_ptr<BinomialNode<T>> traverseA = root;
        shared_ptr<BinomialNode<T>> traverseB = other.root;
        shared_ptr<BinomialNode<T>> traverseC = nullptr;
        shared_ptr<BinomialNode<T>> result = nullptr;

        if(traverseA->degree <= traverseB->degree)
        {
            traverseC = traverseA;
            traverseA = traverseA->next;
        }
        else
        {
            traverseC = traverseB;
            traverseB = traverseB->next;
        }
        result = traverseC;

        while(traverseA != nullptr && traverseB != nullptr)
        {
            if(traverseA->degree <= traverseB->degree)
            {
                make_siblings(traverseC, traverseA);
                traverseA = traverseA->next;
            }
            else
            {
                make_siblings(traverseC, traverseB);
                traverseB = traverseB->next;
            }
            traverseC = traverseC->next;
        }

        while(traverseA != nullptr)
        {
            make_siblings(traverseC, traverseA);
            traverseA = traverseA->next;
            traverseC = traverseC->next;
        }
        while(traverseB != nullptr)
        {
            make_siblings(traverseC, traverseB);
            traverseB = traverseB->next;
            traverseC = traverseC->next;
        }

        traverseC = result;
        shared_ptr<BinomialNode<T>> previous = nullptr;
        shared_ptr<BinomialNode<T>> next = traverseC->next;
        while(next != nullptr)
        {
            if(traverseC->degree != next->degree || (next->next != nullptr && traverseC->degree == next->next->degree))
            {
                previous = traverseC;
                traverseC = next;
            }
            else
            {
                if(comparator(traverseC->data, next->data))
                    make_parent(traverseC, next);
                else
                {
                    if(previous == nullptr)
                        result = next;

                    make_parent(next, traverseC);
                    traverseC = next;
                }
            }
            next = traverseC->next;
        }
        root = result;
        find_min();
        return root;
    }

    shared_ptr<BinomialNode<T>> insert(T data)
    {
        shared_ptr<BinomialNode<T>> to_insert = make_shared<BinomialNode<T>>(data);
        if(root != nullptr)
        {
            BinomialHeap<T, Cmp> to_merge(extract_value);
            to_merge.root = to_insert;
            merge_heap(to_merge);
        }
        else
            root = to_insert;

        if(to_extract == nullptr || comparator(to_insert->data, to_extract->data))
            to_extract = to_insert;

        return to_insert;
    }

    void find_min()
    {
        to_extract = nullptr;
        if(root == nullptr)
            return;

        shared_ptr<BinomialNode<T>> to_compare = root;
        while(to_compare != nullptr)
        {
            if(to_extract == nullptr || comparator(to_compare->data, to_extract->data))
                to_extract = to_compare;
            to_compare = to_compare->next;
        }
    }

    shared_ptr<BinomialNode<T>> extract()
    {
        if(to_extract == nullptr)
            return nullptr;
        else
        {
            if(to_extract->previous != nullptr && to_extract->next != nullptr)
                make_siblings(to_extract->previous, to_extract->next);
            else if(to_extract->previous != nullptr)
                to_extract->previous->next = nullptr;
            else if(to_extract->next != nullptr)
            {
                to_extract->next->previous = nullptr;
                root = to_extract->next;
            }
            else
                root = nullptr;
                

            shared_ptr<BinomialNode<T>> temp = nullptr;
            shared_ptr<BinomialNode<T>> child_traverse = to_extract->child;
            while(child_traverse != nullptr)
            {
                child_traverse->parent = nullptr;

                temp = child_traverse->previous;
                child_traverse->previous = child_traverse->next;
                child_traverse->next = temp;
                if(child_traverse->previous == nullptr)
                    break;
                child_traverse = child_traverse->previous;
            }

            BinomialHeap<T, Cmp> to_merge(extract_value);
            to_merge.root = child_traverse;
            if(child_traverse != nullptr)
                child_traverse->previous = nullptr;
            to_merge.root = child_traverse;
            merge_heap(to_merge);
            shared_ptr<BinomialNode<T>> min = to_extract;
            find_min();
            return min;
        }
    }

    shared_ptr<BinomialNode<T>> find_node(shared_ptr<BinomialNode<T>> current, T value)
    {
        if(current == nullptr)
            return nullptr;

        if(!comparator(current->data, value) && !comparator(value, current->data))
                return current;

        shared_ptr<BinomialNode<T>> depth = find_node(current->child, value);
        if(depth != nullptr)
            return depth;

        return find_node(current->next, value);
    }

    shared_ptr<BinomialNode<T>> change_value(T old_value, T new_value)
    {
        shared_ptr<BinomialNode<T>> old = find_node(root, old_value);
        if(old == nullptr)
            return nullptr;
        
        shared_ptr<BinomialNode<T>> to_change = old;
        to_change->data = new_value;
        shared_ptr<BinomialNode<T>> parent = to_change->parent;
        while(parent != nullptr && comparator(to_change->data, parent->data))
        {
            swap(to_change->data, parent->data);
            to_change = parent;
            parent = parent->parent;
        }
        find_min();
        return to_change;
    }

    shared_ptr<BinomialNode<T>> delete_value(T value)
    {
        change_value(value, extract_value);
        return extract();
    }

    shared_ptr<BinomialNode<T>> delete_node(shared_ptr<BinomialNode<T>> node)
    {
        shared_ptr<BinomialNode<T>> to_change = node;
        to_change->data = extract_value;
        shared_ptr<BinomialNode<T>> parent = to_change->parent;
        while(parent != nullptr && comparator(to_change->data, parent->data))
        {
            swap(to_change->data, parent->data);
            to_change = parent;
            parent = parent->parent;
        }
        to_extract = to_change;
        return extract();
    }

    shared_ptr<BinomialNode<T>> get_min()
    {
        return to_extract;
    }

};

class IntCompare
{
public:
    bool operator()(const int& left, const int& right) const 
    {
        return left > right;
    }
};

class Party;
class Coalition;

class Politician
{
public:
    Politician()
    {
        id = 0;
        party = nullptr;
        popularity = UINT32_MAX;
        gender = 0;
        order = 0;
        name = "";
        binomial_pointer = nullptr;
    }

    Politician(uint32_t _id, const string & _name, uint32_t _popularity, uint8_t _gender, uint64_t _order)
    {
        id = _id;
        party = nullptr;
        popularity = _popularity;
        gender = _gender;
        order = _order;
        name = _name;
        binomial_pointer = nullptr;
    }

    Politician(const Politician& other)
    {
        id = other.id;
        popularity = other.popularity;
        gender = other.gender;
        order = other.order;
        name = other.name;
    }

    Politician& operator=(const Politician& other)
    {
        id = other.id;
        popularity = other.popularity;
        gender = other.gender;
        order = other.order;
        name = other.name;
        return *this;
    }

    void dispose()
    {
        party = nullptr;
        if(binomial_pointer != nullptr)
            binomial_pointer->dispose();
        binomial_pointer = nullptr;
    }

    uint32_t id;
    shared_ptr<Party> party;
    shared_ptr<BinomialNode<shared_ptr<Politician>>> binomial_pointer;
    uint32_t popularity;
    uint64_t order;
    uint8_t gender;
    string name;
};

class PoliticianPopularityComparator
{
public:
    bool operator()(shared_ptr<Politician>& left, shared_ptr<Politician>& right)
    {
        if(left->popularity == right->popularity)
            return left->order < right->order;
        return left->popularity > right->popularity;
    }
};

class Party
{
public:
    Party() : politicians(BinomialHeap<shared_ptr<Politician>, PoliticianPopularityComparator>(make_shared<Politician>()))
    {
        id = 0;
        coalition = nullptr;
        binomial_pointer = nullptr;
        order = 0;
    }

    Party(uint32_t _id)
    : politicians(BinomialHeap<shared_ptr<Politician>, PoliticianPopularityComparator>(make_shared<Politician>()))
    {
        id = _id;
        coalition = nullptr;
        binomial_pointer = nullptr;
        order = 0;
    }

    void dispose()
    {
        coalition = nullptr;
        if(binomial_pointer != nullptr)
            binomial_pointer->dispose();
        binomial_pointer = nullptr;
        politicians.root = nullptr;
    }

    uint32_t id;
    uint64_t order;
    shared_ptr<Coalition> coalition;
    shared_ptr<BinomialNode<shared_ptr<Party>>> binomial_pointer;
    BinomialHeap<shared_ptr<Politician>, PoliticianPopularityComparator> politicians;
};

class PartyPopularityComparator
{
public:
    bool operator()(shared_ptr<Party>& left, shared_ptr<Party>& right)
    {
        uint32_t pop_left = left->politicians.get_min()->data->popularity;
        uint32_t pop_right = right->politicians.get_min()->data->popularity;
        if(pop_left == pop_right)
            return left->order < right->order;

        return pop_left > pop_right;
    }
};

class Coalition
{
public:
    Coalition(size_t _id) : parties(BinomialHeap<shared_ptr<Party>, PartyPopularityComparator>(make_shared<Party>()))
    {
        id = _id;
        shared_ptr<Party> extract_value = make_shared<Party>();
        extract_value->politicians.insert(make_shared<Politician>());
        parties.extract_value = extract_value;
    }
    size_t id;
    BinomialHeap<shared_ptr<Party>, PartyPopularityComparator> parties;
};

class CoalitionIdComparator
{
public:
    bool operator()(const shared_ptr<Coalition>& left, const shared_ptr<Coalition>& right)
    {
        return left->id < right->id;
    }
};

class CPulitzer {
private:
    size_t max_parties;
    size_t max_politicians;

    uint64_t current_order;
    uint64_t current_party_order;
    size_t coalition_count;

    shared_ptr<Politician>* politicians;
    shared_ptr<Party>* parties;
    shared_ptr<Coalition>* coalitions;

    bool party_exist(uint32_t id) const
    {
        if(id < max_parties)
            return parties[id] != nullptr;
        return false;
    }

    void set_party(shared_ptr<Party> party)
    {
        parties[party->id] = party;
    }

    shared_ptr<Party> get_party(shared_ptr<Politician> politician)
    {
        shared_ptr<BinomialNode<shared_ptr<Politician>>> node = politician->binomial_pointer;
        if(node == nullptr)
            return nullptr;

        while(node->parent != nullptr)
            node = node->parent;

        while(node->previous != nullptr)
            node = node->previous;

        return node->data->party;
    }

    shared_ptr<Coalition> get_coalition(shared_ptr<Party> party)
    {
        shared_ptr<BinomialNode<shared_ptr<Party>>> node = party->binomial_pointer;
        if(node == nullptr)
            return nullptr;

        while(node->parent != nullptr)
            node = node->parent;

        while(node->previous != nullptr)
            node = node->previous;

        return node->data->coalition;
    }

    shared_ptr<Coalition> merge_coalitions(shared_ptr<Coalition> left, shared_ptr<Coalition> right)
    {
        shared_ptr<BinomialNode<shared_ptr<Party>>> node = left->parties.merge_heap(right->parties);
        node->data->coalition = left;
        right->parties.root = nullptr;
        coalitions[right->id] = nullptr;
        left->parties.root->data->coalition = left;
        return left;
    }

    void add_party_to_coalition(shared_ptr<Coalition> coalition, shared_ptr<Party> party)
    {
        party->order = current_party_order++;
        shared_ptr<BinomialNode<shared_ptr<Party>>> node = coalition->parties.insert(party);
        party->binomial_pointer = node;
        coalition->parties.root->data->coalition = coalition;
    }

    bool politician_exist(uint32_t id) const
    {
        if(id >= 1 && id < max_politicians)
            return politicians[id] != nullptr;
        return false;
    }

    void set_politician(shared_ptr<Politician> politician)
    {
        if(politician != nullptr)
            politicians[politician->id] = politician;
    }

public:

    CPulitzer (size_t N, size_t P)
    {
        max_parties = N + 1;
        max_politicians = P + 1;
        current_order = 0;
        current_party_order = 0;
        coalition_count = 0;
        parties = new shared_ptr<Party>[max_parties];
        for (size_t i = 0; i < max_parties; i++)
            parties[i] = nullptr;
        politicians = new shared_ptr<Politician>[max_politicians];
        for (size_t i = 0; i < max_politicians; i++)
            politicians[i] = nullptr;
        coalitions = new shared_ptr<Coalition>[max_parties];
        for (size_t i = 0; i < max_parties; i++)
            coalitions[i] = nullptr;
    }
    
    shared_ptr<Party> get_party(uint32_t id)
    {
        if(party_exist(id))
            return parties[id];
        return nullptr;
    }

    shared_ptr<Politician> get_politician(uint32_t id)
    {
        if(politician_exist(id))
            return politicians[id];
        return nullptr;
    }

    bool register_politician ( uint32_t id_party, uint32_t id_politician, const string & name, uint32_t popularity, uint8_t gender )
    {
        if(politician_exist(id_politician))
            return false;

        shared_ptr<Politician> new_politician = make_shared<Politician>(id_politician, name, popularity, gender, current_order++);

        shared_ptr<Party> party = get_party(id_party);
        if(party == nullptr)
        {
            party = make_shared<Party>(id_party);
            set_party(party);
            new_politician->party = party;
        }

        shared_ptr<BinomialNode<shared_ptr<Politician>>> inserted = party->politicians.insert(new_politician);
        new_politician->binomial_pointer = inserted;
        set_politician(new_politician);
        party->politicians.root->data->party = party;
        return true;
    }
    
    bool politician_name ( uint32_t id_politician, string & name ) const
    {
        if(!politician_exist(id_politician))
            return false;

        name = politicians[id_politician]->name;
        return true;
    }
    
    bool politician_gender ( uint32_t id_politician, uint8_t & gender ) const
    {
        if(!politician_exist(id_politician))
            return false;

        gender = politicians[id_politician]->gender;
        return true;
    }
    
    bool politician_popularity ( uint32_t id_politician, uint32_t & popularity ) const
    {
        if(!politician_exist(id_politician))
            return false;

        popularity = politicians[id_politician]->popularity;
        return true;
    }
    
    bool deregister_politician ( uint32_t id_politician )
    {
        shared_ptr<Politician> politician = get_politician(id_politician);
        if(politician == nullptr)
            return false;

        shared_ptr<Party> party = get_party(politician);
        party->politicians.delete_node(politician->binomial_pointer);
        if(party->politicians.root == nullptr)
        {
            parties[party->id] = nullptr;
            party->dispose();
        }
        else
            party->politicians.root->data->party = party;

        politician->dispose();
        politicians[politician->id] = nullptr;
        return true;
    }
    
    bool party_leader ( uint32_t id_party, uint32_t & id_leader ) const
    {
        if(!party_exist(id_party))
            return false;
        
        id_leader = parties[id_party]->politicians.get_min()->data->id;
        return true;
    }

    bool change_popularity ( uint32_t id_politician, uint32_t popularity )
    {
        shared_ptr<Politician> politician = get_politician(id_politician);
        if(politician == nullptr)
            return false;

        shared_ptr<Party> party = get_party(politician);
        shared_ptr<BinomialNode<shared_ptr<Politician>>> root = party->politicians.root;
        shared_ptr<Politician> updated_politician = make_shared<Politician>(*politician.get());
        updated_politician->order = current_order++;
        updated_politician->popularity = popularity;
        shared_ptr<BinomialNode<shared_ptr<Politician>>> res = party->politicians.change_value(politician, updated_politician);
        party->politicians.root->data->party = party;
        return true;
    }
    
    bool sack_leader ( uint32_t id_party )
    {
        shared_ptr<Party> party = get_party(id_party);
        if(party == nullptr)
            return false;

        shared_ptr<BinomialNode<shared_ptr<Politician>>> node = party->politicians.extract();
        if(party->politicians.root == nullptr)
        {
            parties[party->id] = nullptr;
            party->dispose();
        }
        else
            party->politicians.root->data->party = party;
        //node->data->dispose();
        return true;
    }
    
    bool merge_parties ( uint32_t dest_party, uint32_t src_party )
    {
        shared_ptr<Party> destination = get_party(dest_party);
        if(destination == nullptr)
            return false;
        shared_ptr<Party> source = get_party(src_party);
        if(source == nullptr)
            return false;
        if(destination == source)
            return true;
        shared_ptr<BinomialNode<shared_ptr<Politician>>> node = destination->politicians.merge_heap(source->politicians);
        node->data->party = destination;
        source->dispose();
        parties[source->id] = nullptr;
        return true;
    }
    
    bool create_coalition ( uint32_t id_party1, uint32_t id_party2 )
    {
        shared_ptr<Party> party1 = get_party(id_party1);
        if(party1 == nullptr)
            return false;
        shared_ptr<Party> party2 = get_party(id_party2);
        if(party2 == nullptr)
            return false;

        shared_ptr<Coalition> coalition1 = get_coalition(party1);
        shared_ptr<Coalition> coalition2 = get_coalition(party1);
        if(coalition1 != nullptr && coalition2 != nullptr)
        {
            if(coalition1 == coalition2)
                return true;
            merge_coalitions(coalition1, coalition2);
        }
        else if(coalition1 != nullptr && coalition2 == nullptr)
            add_party_to_coalition(coalition1, party2);
        else if(coalition2 != nullptr && coalition1 == nullptr)
            add_party_to_coalition(coalition2, party1);
        else
        {
            coalition1 = make_shared<Coalition>(coalition_count);
            add_party_to_coalition(coalition1, party1);
            add_party_to_coalition(coalition1, party2);
            coalitions[coalition_count++] = coalition1;
        }
        return true;
    }
    
    bool leave_coalition ( uint32_t id_party )
    {
        shared_ptr<Party> party = get_party(id_party);
        if(party == nullptr)
            return false;
        shared_ptr<Coalition> coalition = get_coalition(party);
        if(coalition == nullptr)
            return false;

        coalition->parties.delete_node(party->binomial_pointer);
        if(coalition->parties.root == nullptr)
        {
            coalitions[coalition->id] = nullptr;
        }
        else
            coalition->parties.root->data->coalition = coalition;

        return true;
    }
    
    bool coalition_leader ( uint32_t id_party, uint32_t & id_leader ) const
    {
        if(!party_exist(id_party))
            return false;
        shared_ptr<Party> party = parties[id_party];
        shared_ptr<BinomialNode<shared_ptr<Party>>> node = party->binomial_pointer;
        if(node == nullptr)
            return party_leader(id_party, id_party);

        while(node->parent != nullptr)
            node = node->parent;

        while(node->previous != nullptr)
            node = node->previous;

        shared_ptr<Coalition> coalition = node->data->coalition;
        if(coalition == nullptr)
            return party_leader(id_party, id_party);
        
        shared_ptr<BinomialNode<shared_ptr<Politician>>> pol_node = coalition->parties.get_min()->data->politicians.get_min();
        id_leader = pol_node->data->id;
        return true;
    }
    
    bool scandal_occured ( uint32_t id_party )
    {
        shared_ptr<Party> party = get_party(id_party);
        if(party == nullptr)
            return false;
        shared_ptr<Coalition> coalition = get_coalition(party);
        if(coalition == nullptr)
        {
            sack_leader(id_party);
            return true;
        }
        
        leave_coalition(id_party);
        sack_leader(id_party);
        add_party_to_coalition(coalition, party);
        return true;
    }
        
};

#include <random>

unsigned int* generateNumericParameters(int count)
{
    static std::random_device dev;
    static std::mt19937 rng(dev());
    static std::uniform_int_distribution<std::mt19937::result_type> dist(0,1000100);

    auto out = new unsigned int[count];

    for(int i = 0; i < count; i++)
    {
        out[i] = dist(rng);
    }
    return out;
}

void fuzz()
{
    CPulitzer bot(1000000, 1000000);

    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> method(0,13);


    unsigned int cycles = 0xffffffff;

    uint32_t discardInt;
    uint8_t discardInt2;
    string discardString;

    for(unsigned int i = 0; i < cycles; i++)
    {
        int methodNumber = method(rng);
        unsigned int* params = nullptr;

		switch (methodNumber)
		{
			case 0:
			{
				params = generateNumericParameters(4);
				bot.register_politician(params[0], params[1], "", params[2], params[3]);
				break;
			}
			case 1:
			{
				params = generateNumericParameters(1);
				bot.politician_name(params[0], discardString);
				break;
			}
			case 2:
			{
				params = generateNumericParameters(1);
				bot.politician_gender(params[0], discardInt2);
				break;
			}
			case 3:
			{
				params = generateNumericParameters(1);
				bot.politician_popularity(params[0], discardInt);
				break;
			}
			case 4:
			{
				params = generateNumericParameters(1);
				bot.deregister_politician(params[0]);
				break;
			}
			case 5:
			{
				params = generateNumericParameters(1);
				bot.party_leader(params[0], discardInt);
				break;
			}
			case 6:
			{
				params = generateNumericParameters(1);
				bot.change_popularity(params[0], discardInt);
				break;
			}
			case 7:
			{
				params = generateNumericParameters(1);
				bot.sack_leader(params[0]);
				break;
			}
			case 8:
			{
				params = generateNumericParameters(2);
				bot.merge_parties(params[0], params[1]);
				break;
			}
			case 9:
			{
				params = generateNumericParameters(2);
				bot.create_coalition(params[0], params[1]);
				break;
			}
			case 10:
			{
				params = generateNumericParameters(1);
				bot.leave_coalition(params[0]);
				break;
			}
			case 11:
			{
				params = generateNumericParameters(1);
				bot.coalition_leader(params[0], discardInt);
				break;
			}
			case 12:
			{
				params = generateNumericParameters(1);
				bot.scandal_occured(params[0]);
				break;
			}
		}

        delete[] params;
    }
}

int main (void)
{
    bool result = false;
    uint8_t gender;
    uint32_t popularity, id_leader;
    std::string name;

    CPulitzer bot( 3, 10 );
    result = bot.register_politician( 1, 5, "VK", 1000, 77 ); // true
   result = bot.change_popularity( 5, 300 ); // true

    fuzz();

    /*CPulitzer bot( 3, 10 );
    result = bot.party_leader( 1, id_leader ); // false
    result = bot.register_politician( 1, 5, "VK", 1000, 77 ); // true
    result = bot.register_politician( 2, 4, "MZ", 1000, 77 ); // true
    result = bot.register_politician( 2, 7, "VS", 500, 77 ); // true
    result = bot.party_leader( 1, id_leader ); // true, 5
    result = bot.party_leader( 2, id_leader ); // true, 4
    result = bot.change_popularity( 7, 2000 ); // true
    result = bot.party_leader( 2, id_leader ); // true, 7
    result = bot.leave_coalition(2);
    result = bot.register_politician( 1, 2, "MT", 500, 77 ); // true
    result = bot.register_politician( 2, 2, "JP", 500, 77 ); // false
    result = bot.register_politician( 2, 9, "JP", 500, 77 ); // true
    result = bot.deregister_politician( 5 ); // true
    result = bot.coalition_leader(1, id_leader);
    result = bot.party_leader( 1, id_leader ); // true, 2
    result = bot.scandal_occured(2);
    //result = bot.sack_leader( 2 ); // true
    shared_ptr<Party> party = bot.get_party(2);
    result = bot.change_popularity( 9, 200 ); // true
    result = bot.sack_leader( 1 ); // true*/

    /*CPulitzer bot2( 5, 5 );
    result = bot2.register_politician( 0, 0, "RS", 150, 77 ); // true
    result = bot2.register_politician( 1, 1, "RS", 50, 77 ); // true
    result = bot2.register_politician( 2, 2, "RS", 60, 77 ); // true
    result = bot2.register_politician( 3, 3, "VKml", 100, 77 ); // true
    result = bot2.register_politician( 3, 4, "ZMZ", 50, 70 ); // true
    result = bot2.deregister_politician( 3 ); // true
    result = bot2.merge_parties( 3, 2 ); // true
    result = bot2.merge_parties( 3, 1 ); // true
    result = bot2.party_leader( 0, id_leader ); // true, 0
    result = bot2.party_leader( 1, id_leader ); // false
    result = bot2.party_leader( 2, id_leader ); // false
    result = bot2.party_leader( 3, id_leader ); // true, 2*/

    
    /*CPulitzer bot3( 10, 10 );
    result = bot3.register_politician( 9, 1, "MK", 100, 77 ); // true
    result = bot3.register_politician( 0, 0, "IB", 150, 77 ); // true
    result = bot3.register_politician( 1, 2, "VR", 50, 77 ); // true
    result = bot3.create_coalition( 9, 1 ); // true
    result = bot3.leave_coalition( 1 ); // true
    result = bot3.create_coalition( 0, 1 ); // true
    result = bot3.coalition_leader( 0, id_leader ); // true, 0
    result = bot3.coalition_leader( 1, id_leader ); // true, 0
    result = bot3.coalition_leader( 9, id_leader ); // true, 1
    result = bot3.change_popularity( 2, 200 ); // true
    result = bot3.coalition_leader( 0, id_leader ); // true, 2
    result = bot3.leave_coalition( 9 ); // false*/
    return 0;
}