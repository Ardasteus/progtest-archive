#ifndef __PROGTEST__
#include <cassert>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <string>
#include <memory>
#include <functional>
#include <vector>
#include <algorithm>
using namespace std;
#endif /* __PROGTEST__ */

class Car
{
public:

    Car()
    {
        Owner = nullptr;
        RZ = nullptr;
    };
    Car(const string rz)
    {
        Owner = nullptr;
        RZ = rz;
    }
    string RZ;

    static struct CarComparator
    {
        bool operator() (const Car* left, const Car* right)
        {
            return left->RZ < right->RZ;
        }
    } CarComparator;

    void* Owner;

private:
};

class Person
{
public:

    Person()
    {
        FirstName = nullptr;
        LastName = nullptr;
    };

    Person(const string first, const string last)
    {
        FirstName = first;
        LastName = last;
    }
    string FirstName;
    string LastName;
    vector<Car*> Cars;

    static struct PersonComparator
    {
        bool operator() (const Person * left, const Person * right)
        {
            if(left->LastName == right->LastName)
                return left->FirstName < right->FirstName;

            return left->LastName < right->LastName;
        }
    } PersonComparator;

private:
};

class CCarList
{
public:
    CCarList()
    {
        iterator = 0;
    }

    CCarList(const vector<Car*> * cars)
    {
        iterator = 0;
        Cars = *cars;
    }

    CCarList(const CCarList & list)
    {
        iterator = list.iterator;
        Cars = list.Cars;
    }

    ~CCarList()
    {
        Cars.clear();
        Cars.shrink_to_fit();
    }

    const string & RZ( void ) const
    {
        return Cars[iterator]->RZ;
    }
    bool AtEnd( void ) const
    {
        return iterator == Cars.size();
    }
    void Next( void )
    {
        iterator++;
    }
private:
    size_t iterator;
    vector<Car*> Cars;
};

class CPersonList
{
public:
    CPersonList()
    {
        iterator = 0;
    }

    CPersonList(const vector<Person*> * people)
    {
        iterator = 0;
        People = *people;
    }

    CPersonList(const CPersonList & list)
    {
        iterator = list.iterator;
        People = list.People;
    }

    ~CPersonList()
    {
        People.clear();
        People.shrink_to_fit();
    }
    const string & Name( void ) const
    {
        return People[iterator]->FirstName;
    }
    const string & Surname( void ) const
    {
        return People[iterator]->LastName;
    }
    bool AtEnd( void ) const
    {
        return iterator == People.size();
    }
    void Next( void )
    {
        iterator++;
    }
private:
    size_t iterator;
    vector<Person*> People;
};

class CRegister
{
public:
    CRegister( void );
    ~CRegister( void );
    CRegister  ( const CRegister & src ) = delete;
    CRegister & operator = ( const CRegister & src ) = delete;
    bool AddCar( const string & rz,
                             const string & name,
                             const string & surname );
    bool DelCar( const string & rz );
    bool Transfer( const string & rz,
                             const string & nName,
                             const string & nSurname);
    CCarList ListCars( const string & name,
                             const string & surname ) const;
    int CountCars( const string & name,
                             const string & surname ) const;
    CPersonList ListPersons( void ) const;

    vector<Car*> Cars;
    vector<Person*> People;
private:
};

CRegister::CRegister(void) {
}

CRegister::~CRegister(void) {
    for(auto car : Cars)
    {
        delete car;
    }
    Cars.clear();
    Cars.shrink_to_fit();

    for(auto Person : People)
    {
        Person->Cars.clear();
        Person->Cars.shrink_to_fit();
        delete Person;
    }

    People.clear();
    People.shrink_to_fit();
}

bool CRegister::AddCar(const string &rz, const string &name, const string &surname) 
{
    Car* new_car = new Car(rz);
    bool found = binary_search(Cars.begin(), Cars.end(), new_car, Car::CarComparator);
    if(found)
    {
        delete(new_car);
        return false;
    }

    auto car_index = lower_bound(Cars.begin(), Cars.end(), new_car, Car::CarComparator);
    Cars.insert(car_index, new_car);

    Person* owner = new Person(name, surname);
    found = binary_search(People.begin(), People.end(), owner, Person::PersonComparator);
    auto owner_index = lower_bound(People.begin(), People.end(), owner, Person::PersonComparator);
    if(!found)
        People.insert(owner_index, owner);
    else
    {
        auto aowner_index = distance(People.begin(), owner_index);
        delete(owner);
        owner = People[aowner_index];
    }

    new_car->Owner = owner;
    car_index = lower_bound(owner->Cars.begin(), owner->Cars.end(), new_car, Car::CarComparator);
    owner->Cars.insert(car_index, new_car);

    return true;
}

bool CRegister::DelCar(const string &rz) 
{
    Car car = Car(rz);
    bool found = binary_search(Cars.begin(), Cars.end(), &car, Car::CarComparator);
    if(!found)
    {
        return false;
    }

    auto car_index = lower_bound(Cars.begin(), Cars.end(), &car, Car::CarComparator);
    auto rcar_index = distance(Cars.begin(), car_index);
    Car* to_delete = Cars[rcar_index];
    Person* owner = (Person*)to_delete->Owner;
    Cars.erase(car_index);

    car_index = lower_bound(owner->Cars.begin(), owner->Cars.end(), to_delete, Car::CarComparator);
    owner->Cars.erase(car_index);
    delete to_delete;

    if(owner->Cars.empty())
    {
        auto owner_index = lower_bound(People.begin(), People.end(), owner, Person::PersonComparator);
        People.erase(owner_index);
        delete owner;
    }

    return true;
}

bool CRegister::Transfer(const string &rz, const string &nName, const string &nSurname) 
{
    Car car = Car(rz);
    bool found = binary_search(Cars.begin(), Cars.end(), &car, Car::CarComparator);
    if(!found)
        return false;

    auto car_index = lower_bound(Cars.begin(), Cars.end(), &car, Car::CarComparator);
    auto rcar_index = distance(Cars.begin(), car_index);
    Car* to_update = Cars[rcar_index];
    Person* old_owner = (Person*)to_update->Owner;

    if(old_owner->FirstName == nName && old_owner->LastName == nSurname)
        return false;

    //Find new owner
    Person* owner = new Person(nName, nSurname);
    found = binary_search(People.begin(), People.end(), owner, Person::PersonComparator);
    auto owner_index = lower_bound(People.begin(), People.end(), owner, Person::PersonComparator);
    if(!found)
        People.insert(owner_index, owner);
    else
    {
        auto aowner_index = distance(People.begin(), owner_index);
        delete(owner);
        owner = People[aowner_index];
    }

    //Add car to new owner
    to_update->Owner = owner;
    car_index = lower_bound(owner->Cars.begin(), owner->Cars.end(), to_update, Car::CarComparator);
    owner->Cars.insert(car_index, to_update);

    //Remove car from old owner and remove old owner if needed
    car_index = lower_bound(old_owner->Cars.begin(), old_owner->Cars.end(), to_update, Car::CarComparator);
    old_owner->Cars.erase(car_index);

    if(old_owner->Cars.empty())
    {
        owner_index = lower_bound(People.begin(), People.end(), old_owner, Person::PersonComparator);
        People.erase(owner_index);
        delete old_owner;
    }

    return true;
}

int CRegister::CountCars( const string & name, const string & surname ) const
{
    Person* owner = new Person(name, surname);
    bool found = binary_search(People.begin(), People.end(), owner, Person::PersonComparator);
    auto owner_index = lower_bound(People.begin(), People.end(), owner, Person::PersonComparator);
    delete(owner);
    if(!found)
    {
        return 0;
    }
    else
    {
        auto aowner_index = distance(People.begin(), owner_index);
        owner = People[aowner_index];
        return (int)owner->Cars.size();
    }
}

CCarList CRegister::ListCars( const string & name, const string & surname ) const
{
    Person* owner = new Person(name, surname);
    bool found = binary_search(People.begin(), People.end(), owner, Person::PersonComparator);
    auto owner_index = lower_bound(People.begin(), People.end(), owner, Person::PersonComparator);
    delete(owner);
    if(!found)
    {
        return CCarList();
    }
    else
    {
        auto aowner_index = distance(People.begin(), owner_index);
        owner = People[aowner_index];
        return CCarList(&owner->Cars);
    }
}

CPersonList CRegister::ListPersons( void ) const
{
    return CPersonList(&People);
}

#ifndef __PROGTEST__
 
bool checkPerson ( CRegister & r,const string & name,const string & surname,vector<string> result )
{
    for ( CCarList l = r . ListCars ( name, surname ); ! l . AtEnd (); l . Next () )
    {
        auto pos = find ( result . begin (), result . end (), l . RZ () );
        if ( pos == result . end () )
            return false;
        result . erase ( pos );
    }
    return result . size () == 0;
}

int main ( void )
{
    CRegister b1;

    assert ( b1 . AddCar ( "ABC-12-34", "John", "Smith" ) == true );
    assert ( b1 . AddCar ( "ABC-32-22", "John", "Hacker" ) == true );
    assert ( b1 . AddCar ( "XYZ-11-22", "Peter", "Smith" ) == true );
    assert ( b1 . CountCars ( "John", "Hacker" ) == 1 );
    assert ( checkPerson ( b1, "John", "Hacker", { "ABC-32-22" } ) );
    assert ( b1 . Transfer ( "XYZ-11-22", "John", "Hacker" ) == true );
    assert ( b1 . AddCar ( "XYZ-99-88", "John", "Smith" ) == true );
    assert ( b1 . CountCars ( "John", "Smith" ) == 2 );
    assert ( checkPerson ( b1, "John", "Smith", { "ABC-12-34", "XYZ-99-88" } ) );
    assert ( b1 . CountCars ( "John", "Hacker" ) == 2 );
    assert ( checkPerson ( b1, "John", "Hacker", { "ABC-32-22", "XYZ-11-22" } ) );
    assert ( b1 . CountCars ( "Peter", "Smith" ) == 0 );
    assert ( checkPerson ( b1, "Peter", "Smith", {  } ) );
    assert ( b1 . Transfer ( "XYZ-11-22", "Jane", "Black" ) == true );
    assert ( b1 . CountCars ( "Jane", "Black" ) == 1 );
    assert ( checkPerson ( b1, "Jane", "Black", { "XYZ-11-22" } ) );
    assert ( b1 . CountCars ( "John", "Smith" ) == 2 );
    assert ( checkPerson ( b1, "John", "Smith", { "ABC-12-34", "XYZ-99-88" } ) );
    assert ( b1 . DelCar ( "XYZ-11-22" ) == true );
    assert ( b1 . CountCars ( "Jane", "Black" ) == 0 );
    assert ( checkPerson ( b1, "Jane", "Black", {  } ) );
    assert ( b1 . AddCar ( "XYZ-11-22", "George", "White" ) == true );
    CPersonList i1 = b1 . ListPersons ();
    assert ( ! i1 . AtEnd () && i1 . Surname () == "Hacker" && i1 . Name () == "John" );
    assert ( checkPerson ( b1, "John", "Hacker", { "ABC-32-22" } ) );
    i1 . Next ();
    assert ( ! i1 . AtEnd () && i1 . Surname () == "Smith" && i1 . Name () == "John" );
    assert ( checkPerson ( b1, "John", "Smith", { "ABC-12-34", "XYZ-99-88" } ) );
    i1 . Next ();
    assert ( ! i1 . AtEnd () && i1 . Surname () == "White" && i1 . Name () == "George" );
    assert ( checkPerson ( b1, "George", "White", { "XYZ-11-22" } ) );
    i1 . Next ();
    assert ( i1 . AtEnd () );

    CRegister b2;
    assert ( b2 . AddCar ( "ABC-12-34", "John", "Smith" ) == true );
    assert ( b2 . AddCar ( "ABC-32-22", "John", "Hacker" ) == true );
    assert ( b2 . AddCar ( "XYZ-11-22", "Peter", "Smith" ) == true );
    assert ( b2 . AddCar ( "XYZ-11-22", "Jane", "Black" ) == false );
    assert ( b2 . DelCar ( "AAA-11-11" ) == false );
    assert ( b2 . Transfer ( "BBB-99-99", "John", "Smith" ) == false );
    assert ( b2 . Transfer ( "ABC-12-34", "John", "Smith" ) == false );
    assert ( b2 . CountCars ( "George", "White" ) == 0 );
    assert ( checkPerson ( b2, "George", "White", {  } ) );
    CPersonList i2 = b2 . ListPersons ();
    assert ( ! i2 . AtEnd () && i2 . Surname () == "Hacker" && i2 . Name () == "John" );
    assert ( checkPerson ( b2, "John", "Hacker", { "ABC-32-22" } ) );
    i2 . Next ();
    assert ( ! i2 . AtEnd () && i2 . Surname () == "Smith" && i2 . Name () == "John" );
    assert ( checkPerson ( b2, "John", "Smith", { "ABC-12-34" } ) );
    i2 . Next ();
    assert ( ! i2 . AtEnd () && i2 . Surname () == "Smith" && i2 . Name () == "Peter" );
    assert ( checkPerson ( b2, "Peter", "Smith", { "XYZ-11-22" } ) );
    i2 . Next ();
    assert ( i2 . AtEnd () );

    return 0;
}
#endif /* __PROGTEST__ */
