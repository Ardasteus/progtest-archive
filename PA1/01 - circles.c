#include <stdio.h>
#include <math.h>

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

double getArea(double radius)
{
    return M_PI * (radius * radius);
}

double getDistance(double Ax, double Ay, double Bx, double By)
{
    double x = (Bx - Ax);
    double y = (By - Ay);

    return sqrt(x * x + y * y);
}

struct Circle
{
    double x;
    double y;
    double radius;
    double area;
};

int CreateCircleFromString(char str[1024], Circle *circle)
{
    if(sscanf(str, "%lf %lf %lf", &circle->x, &circle->y, &circle->radius) == 3 && circle->radius > 0)
    {
        circle->area = getArea(circle->radius);
        return 0;
    }
    return 1;
}

int CompareDoubles(double d1, double d2)
{
    return fabs(d1 - d2) < 0.00000000001 * fabs(d1);
}

//Fix this
double GetIntersectionArea(Circle *circle1, Circle *circle2)
{
    double D = getDistance(circle1->x, circle1->y, circle2->x, circle2->y);

    if(D < fabs(circle2->radius - circle1->radius) || CompareDoubles(D, fabs(circle2->radius - circle1->radius)))
        return M_PI * MIN(circle1->radius * circle1->radius, circle2->radius * circle2->radius);

    if(D < circle1->radius + circle2->radius)
    {
        double A_distanceToMiddle = ((circle1->radius * circle1->radius) - (circle2->radius * circle2->radius) + (D * D)) / (2 * D);
        double B_distanceToMiddle = D - A_distanceToMiddle;
        double Middle_Height = sqrt((circle1->radius * circle1->radius) - (A_distanceToMiddle * A_distanceToMiddle));

        double alpha = fmod(atan2(Middle_Height, A_distanceToMiddle) * 2.0 + 2 * M_PI, 2 * M_PI);
        double beta = fmod(atan2(Middle_Height, B_distanceToMiddle) * 2.0 + 2 * M_PI, 2 * M_PI);

        double Area_1 = (circle1->radius * circle1->radius) / 2 * (alpha - sin(alpha));
        double Area_2 = (circle2->radius * circle2->radius) / 2 * (beta - sin(beta));

        return Area_1 + Area_2;
    }
        
    return 0;
}

void ResolveCirclePosition(Circle *circle1, Circle *circle2)
{
    double point_distance = getDistance(circle1->x, circle1->y, circle2->x, circle2->y);
    double intersection = GetIntersectionArea(circle1, circle2);

    if(CompareDoubles(point_distance, circle1->radius + circle2->radius))
        printf("Vnejsi dotyk, zadny prekryv.\n");
    else if(point_distance > circle1->radius + circle2->radius)
        printf("Kruznice lezi vne sebe, zadny prekryv.\n");
    else if(CompareDoubles(circle1->radius, circle2->radius) && point_distance == 0)
        printf("Kruznice splyvaji, prekryv: %lf\n", circle2->area);
    else if(CompareDoubles(point_distance, circle1->radius - circle2->radius))
        printf("Vnitrni dotyk, kruznice #2 lezi uvnitr kruznice #1, prekryv: %lf\n", intersection);
    else if(CompareDoubles(point_distance, circle2->radius - circle1->radius))
        printf("Vnitrni dotyk, kruznice #1 lezi uvnitr kruznice #2, prekryv: %lf\n", intersection);
    else if(point_distance < circle2->radius - circle1->radius)
        printf("Kruznice #1 lezi uvnitr kruznice #2, prekryv: %lf\n", intersection);
    else if(point_distance < circle1->radius - circle2->radius)
        printf("Kruznice #2 lezi uvnitr kruznice #1, prekryv: %lf\n", intersection);
    else
        printf("Kruznice se protinaji, prekryv: %lf\n", intersection);
}

int main(void) 
{
    struct Circle Circle1;
    struct Circle Circle2;
    char input[1024];
    printf("Zadejte parametry kruznice #1:\n");
    fgets(input, sizeof(input), stdin);
    if(CreateCircleFromString(input, &Circle1) != 0)
    {
        printf("Nespravny vstup.\n");
        return 0;
    }
    printf("Zadejte parametry kruznice #2:\n");
    fgets(input, sizeof(input), stdin);
    if(CreateCircleFromString(input, &Circle2) != 0)
    {
        printf("Nespravny vstup.\n");
        return 0;
    }

    ResolveCirclePosition(&Circle1, &Circle2);

    return 0;
}