#include <stdio.h>
#include <stdlib.h>

#define END 2
#define TRUE 1
#define FALSE 0

typedef struct Tile
{
    int Count;
    char Mask;
} Tile;

typedef struct Row
{
    Tile *Tiles;
    unsigned long int TilesCapacity;
    unsigned long int TilesCount;
} Row;

typedef struct Matrix
{
    Row *Rows;
    unsigned long int RowsCapacity;
    unsigned long int RowsCount;
    unsigned long int Width;
} Matrix;

typedef struct String
{
    char *Characters;
    unsigned long int CharactersCapacity;
    unsigned long int CharactersCount;
} String;


// String functions
String StringCreate(const unsigned long int capacity)
{
    String string;

    string.CharactersCapacity = capacity;
    string.CharactersCount = 0;
    string.Characters = (char*)malloc((capacity + 1) * sizeof(char));
    string.Characters[0] = '\0';

    return string;
}

void StringReallocate(String* string, const unsigned long int capacity)
{
    string->CharactersCapacity = capacity;
    string->Characters = (char*)realloc(string->Characters, (capacity + 1) * sizeof(char));
    string->Characters[capacity] = '\0';
}

void StringAppend(String* string, const char character)
{
    if(string->CharactersCount >= string->CharactersCapacity)
        StringReallocate(string, string->CharactersCapacity * 2);
    
    string->Characters[string->CharactersCount++] = character;
    string->Characters[string->CharactersCount] = '\0';
}

void StringDispose(String* string)
{
    if(string->Characters)
        free(string->Characters);
}

int StringFind(String* string, const char find)
{
    if(!string)
        return FALSE;
    for (unsigned long int i = 0; i < string->CharactersCount; i++)
        if(find == string->Characters[i])
            return TRUE;
    
    return FALSE;
}

//Tile functions
Tile TileCreate()
{
    Tile tile;

    tile.Count = 0;
    tile.Mask = '?';

    return tile;
}

void TilePrint(Tile* tile)
{
    if(!tile)
    {
        printf("E");
        return;
    }

    if(tile->Mask == '#')
        printf("%d", tile->Count);
    else if(tile->Mask == '.')
        printf(".");
    else
        printf("E");
}

//Row functions
Row RowCreate(const unsigned long int capacity)
{
    Row row;

    row.TilesCapacity = capacity;
    row.TilesCount = 0;
    row.Tiles = (Tile*)malloc(capacity * sizeof(Tile));

    return row;
}

void RowReallocate(Row* row, const unsigned long int capacity)
{
    row->TilesCapacity = capacity;
    row->Tiles = (Tile*)realloc(row->Tiles, capacity * sizeof(Tile));
}

void RowAdd(Row* row, Tile tile)
{
    if(row->TilesCount >= row->TilesCapacity)
        RowReallocate(row, row->TilesCapacity * 2);

    row->Tiles[row->TilesCount++] = tile;
}

void RowDispose(Row* row)
{
    if(row->Tiles)
        free(row->Tiles);
}

void RowPrint(Row* row)
{
    if(!row)
    {
        printf("Pointer does not exist\n");
        return;
    }
    for (unsigned long int i = 0; i < row->TilesCount; i++)
        TilePrint(&row->Tiles[i]);

    printf("\n");
}

//Matrix functions
Matrix MatrixCreate(const unsigned long int capacity)
{
    Matrix matrix;

    matrix.RowsCapacity = capacity;
    matrix.RowsCount = 0;
    matrix.Rows = (Row*)malloc(capacity * sizeof(Row));

    return matrix;
}

void MatrixReallocate(Matrix* matrix, const unsigned long int capacity)
{
    matrix->RowsCapacity = capacity;
    matrix->Rows = (Row*)realloc(matrix->Rows, capacity * sizeof(Row));
}

void MatrixAdd(Matrix* matrix, Row row)
{
    if(matrix->RowsCount >= matrix->RowsCapacity)
        MatrixReallocate(matrix, matrix->RowsCapacity * 2);

    matrix->Rows[matrix->RowsCount++] = row;
}

void MatrixDispose(Matrix* matrix)
{
    if(matrix->Rows)
    {
        for (unsigned long int i = 0; i < matrix->RowsCount; i++)
            RowDispose(&matrix->Rows[i]);
        
        free(matrix->Rows);
    }
}

void MatrixPrint(Matrix* matrix)
{
    if(!matrix)
    {
        printf("Matrix not found\n");
        return;
    }

    for (unsigned long int i = 0; i < matrix->RowsCount; i++)
        RowPrint(&matrix->Rows[i]);
}

//Input reading functions
int ReadLine(String* string, String* allowed)
{
    if(!string)
        return FALSE;
    while(TRUE)
    {
        char c = getchar();

        if(c == '\n')
        {
            if(string->CharactersCount == 0)
                return END;
            else
                return TRUE;
            
        }

        if(c == EOF)
            return EOF;
        
        if(!allowed || StringFind(allowed, c))
            StringAppend(string, c);
        else
            return FALSE;
    }
}

//Solution functions
int MatrixAppendInputLine(Matrix* matrix)
{
    String filter = StringCreate(2);
    StringAppend(&filter, '\\');
    StringAppend(&filter, '/');

    String string = StringCreate(4);
    int status = ReadLine(&string, &filter);
    StringDispose(&filter);
    if(status == END)
    {
        StringDispose(&string);
        return END;
    }
    else if(status == EOF)
    {
        StringDispose(&string);
        return EOF;
    }
    else if(status)
    {
        if(matrix->RowsCount == 0)
        {
            matrix->Width = string.CharactersCount + 1;
            MatrixAdd(matrix, RowCreate(matrix->Width));
            for (unsigned long int i = 0; i < matrix->Width; i++)
                RowAdd(&matrix->Rows[0], TileCreate());
        }

        if(string.CharactersCount + 1 != matrix->Width)
        {
            StringDispose(&string);
            return FALSE;
        }
        MatrixAdd(matrix, RowCreate(matrix->Width));
        for (unsigned long int i = 0; i < matrix->Width; i++)
            RowAdd(&matrix->Rows[matrix->RowsCount - 1], TileCreate());

        for (unsigned long int i = 0; i < string.CharactersCount; i++)
        {
            if(string.Characters[i] == '\\')
            {
                matrix->Rows[matrix->RowsCount - 2].Tiles[i].Count++;
                matrix->Rows[matrix->RowsCount - 1].Tiles[i + 1].Count++;
            }
            else
            {
                matrix->Rows[matrix->RowsCount - 1].Tiles[i].Count++;
                matrix->Rows[matrix->RowsCount - 2].Tiles[i + 1].Count++;
            }
        }
        
    }
    else
    {
        StringDispose(&string);
        return FALSE;
    }

    StringDispose(&string);
    return TRUE;
}

int GenerateMatrix(Matrix* matrix)
{
    while (TRUE)
    {
        int status = MatrixAppendInputLine(matrix);
        if(status == END)
            break;
        else if(status == FALSE || status == EOF)
            return FALSE;
    }
    
    if(matrix->RowsCount > 0)
        return TRUE;
    else
        return FALSE;
}

int ReadMaskRow(Matrix* matrix, const int y_index)
{
    String filter = StringCreate(2);
    StringAppend(&filter, '#');
    StringAppend(&filter, '.');

    String string = StringCreate(4);
    int status = ReadLine(&string, &filter);
    StringDispose(&filter);
    if(status == END)
    {
        StringDispose(&string);
        return END;
    }
    else if(status == EOF)
    {
        StringDispose(&string);
        return EOF;
    }
    else if(status)
    {
        if(string.CharactersCount != matrix->Width)
        {
            StringDispose(&string);
            return FALSE;
        }


        for (unsigned long int i = 0; i < string.CharactersCount; i++)
        {
            matrix->Rows[y_index].Tiles[i].Mask = string.Characters[i];
        }
    }
    else
    {
        StringDispose(&string);
        return FALSE;
    }

    StringDispose(&string);
    return TRUE;    
}

int GenerateMask(Matrix* matrix)
{
    int status = -1;
    for (unsigned long int i = 0; i < matrix->RowsCount; i++)
    {
        status = ReadMaskRow(matrix, i);
        if(status == END || status == FALSE)
            return FALSE;
    }
    status = ReadMaskRow(matrix, 0);
    if(status == EOF)
        return TRUE;
    else
        return FALSE;
    
}

int main(void)
{
    Matrix matrix = MatrixCreate(2);
    printf("Vypln:\n");

    if(!GenerateMatrix(&matrix))
    {
        MatrixDispose(&matrix);
        printf("Nespravny vstup.\n");
        return 0;
    }

    printf("Maska:\n");

    if(!GenerateMask(&matrix))
    {
        MatrixDispose(&matrix);
        printf("Nespravny vstup.\n");
        return 0;
    }

    MatrixPrint(&matrix);

    MatrixDispose(&matrix);

    return 0;
}