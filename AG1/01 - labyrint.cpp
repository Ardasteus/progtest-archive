#include <iostream>
#include <memory>
#include <math.h>

using namespace std;

class Utility
{
public:
    bool* convert_to_bool_array(short number, short size)
    {
        bool* result = new bool[size];
        for(int i = 0; i < size; i++)
            result[i] = (number >> i & 1) == 1;

        return result;
    }

    short two_to_pow(short power)
    {
        short result = 1;

        for(int i = 0; i < power; i++)
            result *= 2; 

        return result;
    }
};

class Map
{
public:
    Map(short _map_size)
    {
        map_size = _map_size;
    }

    ~Map()
    {
        if(original_map)
        {
            for(int i = 0; i < map_size; i++)
                delete[] original_map[i];
            delete[] original_map;
        }
        if(mask)
        {
            for(int i = 0; i < map_size; i++)
                delete[] mask[i];
            delete[] mask;
        }
    }

    short map_size;
    bool** original_map;
    short** mask;
};

class Lever
{
public:
    Lever(short _distance = -2, short _size = -1)
    {
        size = _size;
        distance = _distance;
    }

    ~Lever()
    {
        if(definition)
            delete[] definition;
    }

    short distance;
    short size;
    bool* definition;
};

class LeverCombination
{
public:
    LeverCombination(short _id, short _lever_count, short _lever_size)
    {
        id = _id;
        lever_count = _lever_count;
        lever_size = _lever_size;
        travel_cost = -1;
        change_vector = nullptr;
    }

    ~LeverCombination()
    {
        if(change_vector)
            delete[] change_vector;
    }

    short id;
    short lever_count;
    short lever_size;
    short travel_cost;
    bool* change_vector;
};

class LeverCollection
{
public:
    LeverCollection(short _map_size, short _lever_count)
    {
        map_size = _map_size;
        lever_count = _lever_count;
    }

    ~LeverCollection()
    {
        if(levers)
            delete[] levers;
    }

    void set_combination(LeverCombination& combination)
    {
        Utility utils;
        short distance = 0;
        bool minus_lever = false;
        bool* combo = utils.convert_to_bool_array(combination.id, lever_count);
        for(int i = 0; i < lever_count; i++)
            if(combo[i])
            {
                if(levers[i].distance == -1)
                    minus_lever = true;
                else if(levers[i].distance > distance)
                    distance = levers[i].distance;

                for(int j = 0; j < map_size; j++)
                    if(levers[i].definition[j])
                        combination.change_vector[j] = !combination.change_vector[j];
            }

        delete[] combo;

        combination.travel_cost = distance * 2;
        if(minus_lever)
            combination.travel_cost += 2;
    }

    short map_size;
    short lever_count;
    Lever* levers;
};

class Point
{
public:
    Point(short _x = -1, short _y = -1)
    {
        x = _x;
        y = _y;
    }

    Point& operator=(const Point& other)
    {
        x = other.x;
        y = other.y;
        return *this;
    }

    short x;
    short y;
};

class PathInfo
{
public:
    PathInfo(Point pos, short cost)
    {
        position = pos;
        path_cost = cost;
        parent = nullptr;
    }

    ~PathInfo()
    {
        parent = nullptr;
    }

    shared_ptr<PathInfo> parent;
    Point position;
    short path_cost;
};

class Node
{
public:
    Node(const Point& pos)
    {
        position = pos;
        next = nullptr;
        path_info = nullptr;
    }

    ~Node()
    {
        next = nullptr;
        path_info = nullptr;
    }

    shared_ptr<Node> next;
    shared_ptr<PathInfo> path_info;
    Point position;
};

class Queue
{
public:
    shared_ptr<Node> first;
    shared_ptr<Node> last;

    Queue()
    {
        first = nullptr;
        last = nullptr;
    }

    ~Queue()
    {
        first = nullptr;
        last = nullptr;
    }

    void enqueue(shared_ptr<Node> to_add)
    {
        if(first == nullptr)
        {
            first = to_add;
            last = first;
        }
        else
        {
            last->next = to_add;
            last = last->next;
        }
    }

    shared_ptr<Node> dequeue()
    {
        if(first == nullptr)
            return nullptr;

        shared_ptr<Node> to_return = first;
        first = first->next;
        return to_return;
    }

    bool is_empty()
    {
        return first == nullptr;
    }
};

class Solution
{
public:
    Solution()
    {
        travel_cost = -1;
        combination_id = 0;
        lever_count = 0;
    }

    void print()
    {
        cout << travel_cost << endl;
        if(travel_cost != -1)
        {
            if(lever_count != 0)
            {
                Utility utils;
                bool* combination = utils.convert_to_bool_array(combination_id, lever_count);
                for(int i = 0; i < lever_count; i++)
                    cout << combination[i];
                delete[] combination;
                cout << endl;
            }

            shared_ptr<PathInfo> current_print = path;
            while(current_print)
            {
                cout << "[" << current_print->position.x + 1 << ";" << current_print->position.y + 1 << "]";
                current_print = current_print->parent;
                if(current_print)
                    cout << ",";
            }
            cout << endl;
        }
    }

    shared_ptr<PathInfo> path;
    short travel_cost;
    short combination_id;
    short lever_count;
};

void parse_levers(LeverCollection& levers, short lever_count, short map_size)
{
    levers.levers = new Lever[lever_count];
    for(int i = 0; i < lever_count; i++){
        char* raw_lever = new char[map_size + 1];
        cin >> levers.levers[i].distance >> raw_lever;
        levers.levers[i].size = map_size;
        levers.levers[i].definition = new bool[map_size];
        for(int j = 0; j < map_size; j++)
            levers.levers[i].definition[j] = raw_lever[j] == '1';

        delete[] raw_lever;
    }
}

void parse_map(Map& map, short map_size)
{
    map.original_map = new bool*[map_size];
    map.mask = new short*[map_size];
    for(int i = 0; i < map_size; i++)
    {
        map.original_map[i] = new bool[map_size];
        map.mask[i] = new short[map_size];
        char* raw_row = new char[map_size + 1];
        cin >> raw_row;
        for(int j = 0; j < map_size; j++)
        {   
            map.original_map[i][j] = raw_row[j] == '1';
            map.mask[i][j] = -1;
        }
        delete[] raw_row;
    }
}

bool is_visited(Map& map, LeverCombination& combination, Point point)
{
    if(point.x >= 0 && point.x < map.map_size && point.y >= 0 && point.y < map.map_size)
    {
        if(combination.change_vector[point.x])
        {
            return !map.original_map[point.y][point.x] || map.mask[point.y][point.x] == combination.id;
        }
        else
            return map.original_map[point.y][point.x] || map.mask[point.y][point.x] == combination.id;
    }
    return true;
}

void set_visited(Map& map, LeverCombination& combination, Point point)
{
    map.mask[point.y][point.x] = combination.id;
}

short calculate_distance(short travel_cost, Point source, Point target)
{
    return (short)(sqrt(pow(source.x - target.x, 2) + pow(source.y- target.y, 2)) + travel_cost);
}

bool try_traverse(Map& map, LeverCombination& combination, Queue& queue, shared_ptr<Node> current_path, Point destination, Point trophy, Solution& solution)
{
    if(is_visited(map, combination, destination))
        return false;

    if(trophy.x == destination.x && trophy.y == destination.y)
    {
        solution.combination_id = combination.id;
        solution.travel_cost = current_path->path_info->path_cost + 1;
        shared_ptr<PathInfo> path = make_shared<PathInfo>(destination, current_path->path_info->path_cost + 1);
        path->parent = current_path->path_info;
        solution.path = path;
        return true;
    }
    else
    {
        short distance_from_dest = calculate_distance(current_path->path_info->path_cost, destination, trophy);
        //short distance_from_current = calculate_distance(current_path.path_cost, position, trophy);
        if(solution.travel_cost == -1 || solution.travel_cost > distance_from_dest)
        {
            set_visited(map, combination, destination);
            shared_ptr<Node> new_node = make_shared<Node>(destination);
            shared_ptr<PathInfo> path = make_shared<PathInfo>(destination, current_path->path_info->path_cost + 1);
            path->parent = current_path->path_info;
            new_node->path_info = path;
            queue.enqueue(new_node); 
        }
    }
    return false;
}

void solve(Map& map, LeverCollection& levers, Point& trophy, short map_size, short lever_count)
{
    Utility utils;
    Solution solution;
    Point target = trophy;
    trophy.x = 0;
    trophy.y = 0;
    solution.lever_count = lever_count;
    short combination_count = utils.two_to_pow(lever_count);
    if(lever_count > 5)
        combination_count = utils.two_to_pow(7);

    for(int i = 0; i < combination_count; i++)
    {
        LeverCombination combination(i, lever_count, map_size);
        combination.change_vector = new bool[map_size];
        for(int j = 0; j < map_size; j++)
            combination.change_vector[j] = false;
        levers.set_combination(combination);
        if(is_visited(map, combination, target) || is_visited(map, combination, trophy))
            continue;
        Queue queue;
        set_visited(map, combination, target);
        shared_ptr<Node> start = make_shared<Node>(target);
        shared_ptr<PathInfo> start_path = make_shared<PathInfo>(target, combination.travel_cost);
        start->path_info = start_path;
        if(target.x == 0 && target.y == 0)
        {
            solution.combination_id = combination.id;
            solution.travel_cost = start->path_info->path_cost;
            solution.path = start_path;
            break;
        }
        queue.enqueue(start);

        while(!queue.is_empty())
        {
            shared_ptr<Node> current = queue.dequeue();
            Point position = current->position;
            if(try_traverse(map, combination, queue, current, Point(position.x + 1, position.y), trophy, solution) ||
            try_traverse(map, combination, queue, current, Point(position.x - 1, position.y), trophy, solution) ||
            try_traverse(map, combination, queue, current, Point(position.x, position.y + 1), trophy, solution) ||
            try_traverse(map, combination, queue, current, Point(position.x, position.y - 1), trophy, solution))
                break;
        }
    }

    solution.print();
}

int main ( void )
{
    short map_size, lever_count;
    cin >> map_size >> lever_count;
    LeverCollection levers(map_size, lever_count);
    parse_levers(levers, lever_count, map_size);
    Map map(map_size);
    parse_map(map, map_size);
    Point trophy;
    cin >> trophy.x >> trophy.y;
    trophy.x--;
    trophy.y--;
    solve(map, levers, trophy, map_size, lever_count);
}
