#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <memory>
#include <functional>
#include <stdexcept>
#include "ipaddress.h"
using namespace std;
#endif /* __PROGTEST__ */

/**
 * Abstract class that all other Records derive from.
 * Provides some basic functionality to prevent code repetition.
 * Most of the explanation of what methods do and reasoning behind them will be done here.
 */
class BaseRecord
{
public:
    /**
     * Basic constructor - initializes an empty Record.
     */
    BaseRecord()
    {
        ID = 0;
        IsUnique = false;
    }

    /**
     * Constructor with parameters.
     * @param const string& name - Name of the Record
     * @param const string& type - Type of the Record. This is to be set explicitly by the records constructor (For example Record A will always set this to "A")
     * @param bool unique - Determiness if Record is unique by name. Only used by CZone and CName, since they have to be unique in the DNS
     */
    BaseRecord(const string& name, const string& type, bool unique)
    {
        ID = 0;
        RecordName = name;
        RecordType = type;
        IsUnique = unique;
    }

    /**
     * Copy constructor
     * @param const BaseRecord& other - Record to copy from
     */ 
    BaseRecord(const BaseRecord& other)
    {
        ID = other.ID;
        RecordName = other.RecordName;
        RecordType = other.RecordType;
        IsUnique = other.IsUnique;
    }

    /**
     * Pure virtual destructor
     */ 
    virtual ~BaseRecord(){};

    /**
     * Get method for Name of the Record
     * @returns const string - Name of the Record
     */ 
    const string Name() const
    {
        return RecordName;
    }

    /**
     * Get method for Type of the Record
     * @returns const string - Type of the Record
     */ 
    const string Type() const
    {
        return RecordType;
    }

    /**
     * Creates a pointer copy of the instance it is called on.
     * Since records are stored as pointers in this DNS system, it is required to create a pointer to a copy of this instance, so it can be stored.
     * @returns BaseReocrd* - Pointer to the copy
     */ 
    virtual BaseRecord* CreateCopy() const 
    {
        return new BaseRecord(*this);
    };

    /**
     * Creates a pointer copy of the instance it is called on. Specifically for comparison
     * This method is usually same as the CreateCopy one. In some cases however it is required to only have certain fields from the Record for succesful comparison.
     * Pretty ugly solution, I know.
     * @returns BaseReocrd* - Pointer to the copy
     */ 
    virtual BaseRecord* CreateCompareCopy() const 
    {
        return new BaseRecord(*this);
    };

    /**
     * Compares this instance with some other record.
     * Derived clases have to override this and cast the other to their type (this is checked before calling the compare function) to ensure correct comparison.
     * @param const BaseRecord* other - instance of a class that we want to compare
     * @returns bool - result of comparison
     */ 
    virtual bool Compare(const BaseRecord* other) const 
    { 
        return RecordName < other->RecordName; 
    };

    /**
     * Overload of << operator (output to stream).
     * Different types of records print differently, this is thus handled by calling the PrintRecord and passing it the output stream
     * This overload basically just acts like a passthrough for the PrintRecord method, that means we can overload the operator in only one place.
     * @param std::ostream& os - output stream to modify
     * @param const BaseRecord& record - record to be printed.
     * @returns std::ostream& - modifed output stream
     */ 
    friend std::ostream & operator<<( std::ostream& os, const BaseRecord& record )
    {
        record.PrintRecord(os);
        return os;
    }

    /**
     * Convert a record to string.
     * Basically the same implementation as PrintRecord, just outputs string.
     * Used in a method that exports lines in CZone class
     * @return string 
     */
    virtual const string ToString() const
    {
        return RecordName + " " + RecordType;
    }

    /**
     * ID is used to preserve insert order.
     * Unique indicates if a record is unique by name. There cannot be 2 records of the same name.
     */
    unsigned long long ID;
    bool IsUnique;

protected:
    /**
     * Name and Type of the Record
     */ 
    string RecordName;
    string RecordType;

    /**
     * Modifies a given output stream and prints out string representation of the record.
     * @param ostream& os - output stream to modify
     */
    virtual void PrintRecord(ostream& os) const
    {
        os << RecordName << " " << RecordType;
    };
};

/**
 * Class that represents an A record in DNS Zone
 */ 
class CRecA : public BaseRecord
{
public:
     /**
     * Constructor with parameters.
     * Creates a new instance of this Record type with given parameters and fills the Type and Unique paramater of the BaseRecord class
     * @param const string& name - Name of the record
     * @param const CIPv4& address - Address of the added A record
     */  
    CRecA(const string& name, const CIPv4& address) : BaseRecord(name, "A", false)
    {
        RecordAddress = address;
    }

    /**
     * Copy constructor
     * @param const CRecA& other - Record to copy from
     */  
    CRecA(const CRecA& other) : BaseRecord(other)
    {
        RecordAddress = other.RecordAddress;
    }

    /**
     * Creates pointer copy
     * @return BaseRecord* - pointer copy
     */
    BaseRecord* CreateCopy() const override
    {
        CRecA* copy = new CRecA(*this);
        return copy;
    }

    /**
     * Creates pointer copy for comparison.
     * @return BaseRecord* - pointer copy
     */
    BaseRecord* CreateCompareCopy() const override
    {
        CRecA* copy = new CRecA(*this);
        return copy;
    };

    /**
     * Compares this record with another record of the same type. The check for type is and should be done before calling this method.
     * @return bool - result of comparison
     */
    bool Compare(const BaseRecord* other) const override
    {
        const CRecA* casted = dynamic_cast<const CRecA*>(other);
        ostringstream stream;
        ostringstream otherstream;
        stream << RecordAddress;
        otherstream << casted->RecordAddress;
        return stream.str() < otherstream.str();
    }

    /**
     * Creates a string representation of this record.
     * @return const string - string representation of record
     */
    const string ToString() const override
    {
        ostringstream temp;
        temp << RecordAddress;
        return RecordName + " " + RecordType + " " + temp.str();
    }

protected:
    CIPv4 RecordAddress;

    /**
     * Prints the string representation of this record to the given output stream
     * @param ostream& stream - output stream
     */ 
    void PrintRecord(ostream& stream) const override
    {
        stream << RecordName << " " << RecordType << " " << RecordAddress;
    }
};

/**
 * Class that represents an AAAA record in DNS Zone
 */ 
class CRecAAAA : public BaseRecord
{
public:
    /**
     * Constructor with parameters.
     * Creates a new instance of this Record type with given parameters and fills the Type and Unique paramater of the BaseRecord class.
     * @param const string& name - Name of the record
     * @param const CIPv6& address - Address of the added AAAA record
     */ 
    CRecAAAA(const string& name, const CIPv6& address) : BaseRecord(name, "AAAA", false)
    {
        RecordAddress = address;
    }
    
    /**
     * Copy constructor
     * @param const CRecAAAA& other - Record to copy from
     */ 
    CRecAAAA(const CRecAAAA& other) : BaseRecord(other)
    {
        RecordAddress = other.RecordAddress;
    }

    /**
     * Creates pointer copy
     * @return BaseRecord* - pointer copy
     */
    BaseRecord* CreateCopy() const override
    {
        CRecAAAA* copy = new CRecAAAA(*this);
        return copy;
    }

    /**
     * Creates pointer copy for comparison.
     * @return BaseRecord* - pointer copy
     */
    BaseRecord* CreateCompareCopy() const override
    {
        CRecAAAA* copy = new CRecAAAA(*this);
        return copy;
    };

    /**
     * Compares this record with another record of the same type. The check for type is and should be done before calling this method.
     * @return bool - result of comparison
     */
    bool Compare(const BaseRecord* other) const override
    {
        const CRecAAAA* casted = dynamic_cast<const CRecAAAA*>(other);
        ostringstream stream;
        ostringstream otherstream;
        stream << RecordAddress;
        otherstream << casted->RecordAddress;
        return stream.str() < otherstream.str();
    }

    /**
     * Creates a string representation of this record.
     * @return const string - string representation of record
     */
    const string ToString() const override
    {
        ostringstream temp;
        temp << RecordAddress;
        return RecordName + " " + RecordType + " " + temp.str();
    }

protected:
    CIPv6 RecordAddress;
    /**
     * Prints the string representation of this record to the given output stream
     * @param ostream& stream - output stream
     */
    void PrintRecord(ostream& stream) const override
    {
        stream << RecordName << " " << RecordType << " " << RecordAddress;
    }
};

/**
 * Class that represents an MX record in DNS Zone
 */ 
class CRecMX : public BaseRecord
{
public:
    /**
     * Constructor with parameters.
     * Creates a new instance of this Record type with given parameters and fills the Type and Unique paramater of the BaseRecord class.
     * @param const string& name - Name of the record
     * @param const string mailServerName - name of the mail server
     * @param const int priority - priority of MX record
     */ 
    CRecMX(const string& name, const string mailServerName, const int priority) : BaseRecord(name, "MX", false)
    {
        RecordPriority = priority;
        RecordMailServerName = mailServerName;
    }

    /**
     * Copy constructor
     * @param const CRecMX& other - Record to copy from
     */ 
    CRecMX(const CRecMX& other) : BaseRecord(other)
    {
        RecordMailServerName = other.RecordMailServerName;
        RecordPriority = other.RecordPriority;
    }

    /**
     * Creates pointer copy
     * @return BaseRecord* - pointer copy
     */
    BaseRecord* CreateCopy() const override
    {
        CRecMX* copy = new CRecMX(*this);
        return copy;
    }

    /**
     * Creates pointer copy
     * @return BaseRecord* - pointer copy
     */
    BaseRecord* CreateCompareCopy() const override
    {
        CRecMX* copy = new CRecMX(*this);
        return copy;
    };

    /**
     * Compares this record with another record of the same type. The check for type is and should be done before calling this method.
     * @return bool - result of comparison
     */
   bool Compare(const BaseRecord* other) const override
    {
        const CRecMX* casted = dynamic_cast<const CRecMX*>(other);
        if(RecordMailServerName == casted->RecordMailServerName)
            return RecordPriority < casted->RecordPriority;

        return RecordMailServerName < casted->RecordMailServerName;
    }

    /**
     * Creates a string representation of this record.
     * @return const string - string representation of record
     */
    const string ToString() const override
    {
        return RecordName + " " + RecordType + " " + to_string(RecordPriority) + " " + RecordMailServerName;
    }

protected:
    int RecordPriority;
    string RecordMailServerName;

    /**
     * Prints the string representation of this record to the given output stream
     * @param ostream& stream - output stream
     */
    void PrintRecord(ostream& stream) const override
    {
        stream << RecordName << " " << RecordType << " " << RecordPriority << " " << RecordMailServerName;
    }
};

/**
 * Class that represents an CNAME record in DNS Zone
 */ 
class CRecCNAME : public BaseRecord
{
public:
    /**
     * Constructor with parameters.
     * Creates a new instance of this Record type with given parameters and fills the Type and Unique paramater of the BaseRecord class.
     * CName is one of two records that have to be unique by name in a DNS zone
     * @param const string& name - Name of the record
     * @param const string alias - alias of another record
     */ 
    CRecCNAME(const string& name, string alias) : BaseRecord(name, "CNAME", true)
    {
        AliasedRecord = alias;
    }

    /**
     * Copy constructor
     * @param const CRecCNAME& other - Record to copy from
     */ 
    CRecCNAME(const CRecCNAME& other) : BaseRecord(other)
    {
        AliasedRecord = other.AliasedRecord;
    }

    /**
     * Creates pointer copy
     * @return BaseRecord* - pointer copy
     */
    BaseRecord* CreateCopy() const override
    {
        CRecCNAME* copy = new CRecCNAME(*this);
        return copy;
    }

    /**
     * Creates pointer copy for comparison.
     * @return BaseRecord* - pointer copy
     */
    BaseRecord* CreateCompareCopy() const override
    {
        CRecCNAME* copy = new CRecCNAME(*this);
        return copy;
    };

    /**
     * Compares this record with another record of the same type. The check for type is and should be done before calling this method.
     * @return bool - result of comparison
     */
    bool Compare(const BaseRecord* other) const override
    {
        const CRecCNAME* casted = dynamic_cast<const CRecCNAME*>(other);
        return RecordName < casted->RecordName;
    }

    /**
     * Creates a string representation of this record.
     * @return const string - string representation of record
     */
    const string ToString() const override
    {
        return RecordName + " " + RecordType + " " + AliasedRecord;
    }

protected:
    string AliasedRecord;

    /**
     * Prints the string representation of this record to the given output stream
     * @param ostream& stream - output stream
     */
    void PrintRecord(ostream& stream) const override
    {
        stream << RecordName << " " << RecordType << " " << AliasedRecord;
    }
};

/**
 * Class that represents an SPF record in DNS Zone
 */ 
class CRecSPF : public BaseRecord
{
public:
    /**
     * Constructor with parameters.
     * Creates a new instance of this Record type with given parameters and fills the Type and Unique paramater of the BaseRecord class.
     * @param const string& name - Name of the record
     */ 
    CRecSPF(const string& name) : BaseRecord(name, "SPF", false)
    {

    }

    /**
     * Copy constructor
     * @param const CRecSPF& other - Record to copy from
     */ 
    CRecSPF(const CRecSPF& other) : BaseRecord(other)
    {
        for(auto address : other.Addresses)
            Addresses.push_back(address);
    }

    /**
     * Creates pointer copy
     * @return BaseRecord* - pointer copy
     */
    BaseRecord* CreateCopy() const override
    {
        CRecSPF* copy = new CRecSPF(*this);
        return copy;
    }

    /**
     * Creates pointer copy for comparison.
     * @return BaseRecord* - pointer copy
     */
    BaseRecord* CreateCompareCopy() const override
    {
        CRecSPF* copy = new CRecSPF(RecordName);
        return copy;
    };

    /**
     * Compares this record with another record of the same type. The check for type is and should be done before calling this method.
     * @return bool - result of comparison
     */
   bool Compare(const BaseRecord* other) const override
    {
        const CRecSPF* casted = dynamic_cast<const CRecSPF*>(other);
        return RecordName < casted->RecordName;
    }

    /**
     * Adds an address to MX record
     * @param const string& address - address to add
     * @return CRecSPF& - returns itself, so you are able to chain Add
     */ 
    CRecSPF& Add(const string& address)
    {
        Addresses.push_back(address);
        return *this;
    }

    /**
     * Creates a string representation of this record.
     * @return const string - string representation of record
     */
    const string ToString() const override
    {
        string result;
        result += RecordName + " " + RecordType;
        if(Addresses.size() != 0)
        {
            result += " ";
            for (size_t i = 0; i < Addresses.size() - 1; i++)
                result += Addresses[i] + ", ";
                
            result += Addresses[Addresses.size() - 1];
        }
        return result;
    }

protected:
    vector<string> Addresses;

    /**
     * Prints the string representation of this record to the given output stream
     * @param ostream& stream - output stream
     */
    void PrintRecord(ostream& stream) const override
    {
        stream << RecordName << " " << RecordType;
        if(Addresses.size() != 0)
        {
            stream << " ";
            for (size_t i = 0; i < Addresses.size() - 1; i++)
                stream << Addresses[i] << ", ";

            stream << Addresses[Addresses.size() - 1];
        }
    }
};

/**
 * Class that acts as a collection of searched Records (CZone::Search, RecordCluster::GetCollection)
 * Enables the user to access records and also print the string representation of all included records.
 */ 
class RecordCollection
{
public:
    /**
     * Default constructor. Initializes empty collection
     */ 
    RecordCollection()
    {
        RecordCount = 0;
    }

    /**
     * Destructor
     * Does not actually delete any pointers, that is handled by CZone and RecordClusters. 
     * This collection works with the pointers provided from CZone itelf, thus deleting pointers here would break the program.
     */ 
    ~RecordCollection()
    {
        Records.clear();
    }


    /**
     * Returns the count of Records in the collection
     * @return int - count of records
     */ 
    int Count() const
    {
        return RecordCount;
    }

    /**
     * Adds a record to the Collection.
     * This is supposed to be called only by RecordCluster.
     * Calling it on its own would most likely result in memory leaks, unless the user handled the removal of added items.
     * This was however not needed for a solution
     * @param BaseRecord* record - record to be added
     */
    void Add(BaseRecord* record)
    {
        RecordCount++;
        Records.push_back(record);
    }

    /**
     * Overloaded [] operator. Enables the user to get a reference to a record from collection
     * @param int index
     * @return BaseRecord&
     */
    BaseRecord& operator[] (int index)
    {
        if(index >= RecordCount || index < 0)
            throw std::out_of_range(std::to_string(index));
        return *Records[index];
    }

    /**
     * Prints out string representation of all included records.
     * Some special cases for CZone are required to work properly
     * @param std::ostream& os - output stream
     * @param const RecordCollection& collection - collection to print
     * @return std::ostream&
     */
    friend std::ostream & operator<<( std::ostream& os, const RecordCollection& collection )
    {
        if(collection.RecordCount <= 0)
            return os;

        for (int i = 0; i < collection.RecordCount - 1; i++)
        {
            BaseRecord* record = collection.Records[i];
            os << *record;
            if(record->Type() != "CZONE")
                os << endl;
        }
        
        BaseRecord* record = collection.Records[collection.RecordCount - 1];
        os << *record;
        if(record->Type() != "CZONE")
            os << endl;

        return os;
    }

private:
    int RecordCount;
    vector<BaseRecord*> Records;
};

/**
 * Comparator used to compare records.
 * If two records are of same type it calls BaseRecord::Compare on one of them
 * If one of the records is unique or they are of different type they are compared by their type
 * @param const BaseRecord* left
 * @param const BaseRecord* right
 */ 
struct RecordComparator
{
public:
    bool operator() (const BaseRecord* left, const BaseRecord* right) const
    {
        if(left->Type() == right->Type() && !left->IsUnique && !right->IsUnique)
            return left->Compare(right);
        
        return left->Type() < right->Type();
    }
};

/**
 * Compares records solely based on their ID.
 * This is used to preserve the order they were inserted in.
 * @param const BaseRecord* left
 * @param const BaseRecord* right
 */ 
struct RecordIdComparator
{
public:
    bool operator() (const BaseRecord* left, const BaseRecord* right) const
    {
        return left->ID < right->ID;
    }
};

/**
 * Collection of records grouped by their name
 * This allows us to effectively search for all records by name (CZone::Search)
 */ 
class RecordCluster
{
public:
    /**
     * Constructor that initializes new cluster with given name
     * @param const string& name - name of the cluster
     */ 
    RecordCluster(const string& name)
    {
        ClusterName = name;
    }

    /**
     * Copy constructor
     * @param const RecordCluster& other
     */ 
    RecordCluster(const RecordCluster& other)
    {
        ClusterName = other.ClusterName;
        for(auto record : other.Records)
        {
            BaseRecord* new_record = record->CreateCopy();
            Records.insert(new_record);
            RawRecords.insert(new_record);
        }
    }

    /**
     * Destructor
     * This destructor is responsible for deleting all the records saved by the program
     */
    ~RecordCluster()
    {
        for(auto record : Records)
            delete(record);
        Records.clear();
    }

    /**
     * Get method for the cluster name
     */
    const string Name() const
    {
        return ClusterName;
    }

    /**
     * Checks if a given record exists in this cluster
     * @param BaseRecord* record
     * @return bool - returns whether the record was found or not 
     */
    bool Find(BaseRecord* record) const
    {
        if(record->Name() != ClusterName)
            return false;

        auto found = Records.find(record);
        return found != Records.end();
    }

    /**
     * Gets a record from the cluster, if it exists.
     * Otherwise it returns nullptr
     * @param const BaseRecord& rec - record to get
     * @return BaseRecord* - pointer to found record or nullptr
     */
    BaseRecord* Get(const BaseRecord& rec)
    {
        BaseRecord* record = rec.CreateCopy();
        auto found = Records.find(record);
        delete(record);
        if(found == Records.end())
            return nullptr;
        
        auto index = distance(Records.begin(), found);
        record = *next(Records.begin(), index);
        return record;
    }

    /**
     * Adds a record to cluster
     * @param BaseRecord* record - record to add
     */
    void Add(BaseRecord* record)
    {
        Records.insert(record);
        RawRecords.insert(record);
    }

    /**
     * Deletes a record from cluster
     * Check if it exists is done before this is called
     * @param BaseRecord* record - record to delete
     */
    void Delete(BaseRecord* record)
    {
        Records.erase(record);
        RawRecords.erase(record);
    }

    /**
     * Returns true if cluster is empty, otherwise false
     * @return bool
     */
    bool Empty() const
    {
        return Records.size() == 0;
    }

    /**
     * Creates a RecordCollection from this cluster.
     * Records are added to the Collection based on how they were inserted
     * @return RecordCollection
     */
    RecordCollection GetCollection() const
    {
        RecordCollection collection = RecordCollection();

        for(auto item : RawRecords)
            collection.Add(item);

        return collection;
    }

    /**
     * Get method that returns records ordered by how they were added
     * @return set<BaseRecord*, RecordIdComparator>& - ordered set by ID
     */
    set<BaseRecord*, RecordIdComparator>& GetRawRecords()
    {
        return RawRecords;
    }

private:
    set<BaseRecord*, RecordComparator> Records;    
    set<BaseRecord*, RecordIdComparator> RawRecords; 
    string ClusterName;
};

/**
 * Compares record clusters based on their name.
 * Used in CZone to effectively search through the clusters based on name
 * @param const RecordCluster* left
 * @param const RecordCluster* right
 */ 
struct RecordClusterComparator
{
public:
    bool operator() (const RecordCluster* left, const RecordCluster* right) const
    {
        return left->Name() < right->Name();
    }
};

/**
 * Class that represents DNS Zone.
 * Supports adding removing and searching for records. Also supports nested Zones.
 */ 
class CZone : public BaseRecord
{
public:
    /**
     * Constructor with parameters.
     * Creates a new instance of this Record type with given parameters and fills the Type and Unique paramater of the BaseRecord class.
     * @param const string& name - Name of the record
     */ 
    CZone(const string& name) : BaseRecord(name, "CZONE", true)
    {
        RecordRawCount = 0;
    }

    /**
     * Copy constructor
     * @param const CZone& other - Record to copy from
     */ 
    CZone(const CZone& other) : BaseRecord(other)
    {
        RecordRawCount = other.RecordRawCount;
        for(auto cluster : other.Clusters)
        {
            RecordCluster* new_cluster = new RecordCluster(*cluster);
            Clusters.insert(new_cluster);
            for(auto record : new_cluster->GetRawRecords())
                Records.insert(record);
        }
    }

    /**
     * Destructor
     * Disposes of clusters
     */ 
    ~CZone()
    {
        for(auto cluster : Clusters)
            delete(cluster);
        Clusters.clear();
        Records.clear();
    }

    /**
     * Creates pointer copy
     * @return BaseRecord* - pointer copy
     */
    BaseRecord* CreateCopy() const override
    {
        CZone* copy = new CZone(*this);
        return copy;
    }

    /**
     * Creates pointer copy for comparison.
     * @return BaseRecord* - pointer copy
     */
    BaseRecord* CreateCompareCopy() const override
    {
        CZone* copy = new CZone(RecordName);
        return copy;
    };

    /**
     * Compares this record with another record of the same type. The check for type is and should be done before calling this method.
     * @return bool - result of comparison
     */
   bool Compare(const BaseRecord* other) const override
    {
        const CZone* casted = dynamic_cast<const CZone*>(other);
        return RecordName < casted->RecordName;
    }    

    /**
     * Overloaded operator =
     * Disposes of data and rebuilds the object, copying from the other object
     * @param const CZone& other - object to copy from
     * @return CZone& - returns itself
     */  
    CZone& operator=(const CZone& other)
    {
        CZone* copy = new CZone(other);

        for(auto cluster : Clusters)
            delete(cluster);
        Clusters.clear();
        Records.clear();
        
        RecordRawCount = copy->RecordRawCount;
        RecordName = copy->RecordName;
        for(auto cluster : copy->Clusters)
        {
            RecordCluster* new_cluster = new RecordCluster(*cluster);
            Clusters.insert(new_cluster);
            for(auto record : new_cluster->GetRawRecords())
                Records.insert(record);
        }

        delete(copy);
        return *this;
    }

    /**
     * Adds a record to a Zone
     * @param const BaseRecord& to_add - record to add
     * @return bool - returns whether the add was succesful or not
     */
    bool Add(const BaseRecord& to_add)
    {
        BaseRecord* record = to_add.CreateCompareCopy();

        RecordCluster* cluster = new RecordCluster(to_add.Name());
        auto cluster_found = Clusters.find(cluster);
        if(cluster_found != Clusters.end())
        {
            delete(cluster);

            if(to_add.IsUnique)
            {
                delete(record);
                return false;
            }

            cluster = *cluster_found;

            if(cluster->GetCollection()[0].IsUnique)
            {
                delete(record);
                return false;
            }

            bool record_exists = cluster->Find(record);
            if(record_exists)
            {
                delete(record);
                return false;
            }
        }

        delete(record);
        record = to_add.CreateCopy();

        record->ID = RecordRawCount++;
        Clusters.insert(cluster);
        cluster->Add(record);
        Records.insert(record);

        return true;
    }

    /**
     * Delete a record from Zone
     * @param const BaseRecord& to_delete - record to delete
     * @return bool - returns whether the removal was succesful or not
     */
    bool Del(const BaseRecord& to_delete)
    {
        BaseRecord* record = nullptr;

        RecordCluster* cluster = new RecordCluster(to_delete.Name());
        auto cluster_found = Clusters.find(cluster);
        delete(cluster);

        if(cluster_found == Clusters.end())
            return false;

        cluster = *cluster_found;

        record = cluster->Get(to_delete);
        if(record == nullptr)
            return false;

        Records.erase(record);
        cluster->Delete(record);
        if(cluster->Empty())
        {
            Clusters.erase(cluster);
            delete(cluster);
        }
        delete(record);
        return true;
    }

    /**
     * Searches for all records with given name. Can also accept a string in format "foo.bar.xyz" to search for a zone.
     * If it searches for a zone the RecordCollection will have one element which will be the zone
     * @param const string& name - Name to search by
     * @return RecordCollection - Collection of found records, empty if none found.
     */
    RecordCollection Search(const string& name) const
    {
        RecordCollection collection = RecordCollection();

        RecordCluster* cluster = new RecordCluster(name);
        auto cluster_found = Clusters.find(cluster);
        delete(cluster);
        if(cluster_found != Clusters.end())
        {
            cluster = *cluster_found;
            return cluster->GetCollection();
        }
        
        auto last_dot = name.find_last_of(".");
        string temp = name;
        string leftover = temp.substr(0, last_dot);
        temp.erase(0, last_dot + 1);
        string search = temp;

        cluster = new RecordCluster(search);
        auto zone_cluster_found = Clusters.find(cluster);
        delete(cluster);
        if(zone_cluster_found != Clusters.end())
        {
            cluster = *zone_cluster_found;
            RecordCollection collection = cluster->GetCollection();
            if(collection.Count() == 1 && collection[0].Type() == "CZONE")
            {
                CZone& zone = dynamic_cast<CZone&>(collection[0]);
                return zone.Search(leftover);
            }
            
        }

        return collection;
    }

    /**
     * Creates a string representation of this record.
     * @return const string - string representation of record
     */
    const string ToString() const override
    {
        vector<string> lines = ExportLines();
        string result;
        for(string line : lines)
            result += line + "\n";

        return result;
    }

protected:

    /**
     * Creates a vector of strings, where each string is one line of a normal string representation of a CZone.
     * Needed for PrintRecord to properly work.
     */
    vector<string> ExportLines() const
    {
        vector<string> lines;

        lines.push_back(RecordName);

        if(Records.empty())
            return lines;

        auto iterator = Records.begin();
        auto end = Records.end();
        end--;
        while (iterator != end)
        {
            BaseRecord* record = *iterator++;
            if(record->Type() != "CZONE")
                lines.push_back(" +- " + record->ToString());
            else
            {
                CZone* zone = dynamic_cast<CZone*>(record);
                vector<string> zone_lines = zone->ExportLines();
                lines.push_back(" +- " + zone_lines[0]);
                for (size_t i = 1; i < zone_lines.size(); i++)
                    lines.push_back(" | " + zone_lines[i]);
            }
        }
        
        BaseRecord* record = *iterator;
        if(record->Type() != "CZONE")
            lines.push_back(" \\- " + record->ToString());
        else
        {
            CZone* zone = dynamic_cast<CZone*>(record);
            vector<string> zone_lines = zone->ExportLines();
            lines.push_back(" \\- " + zone_lines[0]);
            for (size_t i = 1; i < zone_lines.size(); i++)
                lines.push_back("   " + zone_lines[i]);
        }

        return lines;
    }

    /**
     * Prints the string representation of this record to the given output stream
     * Bit of a modified version for CZone to support nested CZones (by using ExportLines method)
     * @param ostream& stream - output stream
     */
    void PrintRecord(ostream& os) const override
    {
        os << RecordName << endl;
        
        if(Records.empty())
            return;

        auto iterator = Records.begin();
        auto end = Records.end();
        end--;
        while (iterator != end)
        {
            BaseRecord* record = *iterator++;
            if(record->Type() != "CZONE")
                os << " +- " << *record << endl;
            else
            {
                CZone* zone = dynamic_cast<CZone*>(record);
                vector<string> zone_lines = zone->ExportLines();
                os << " +- " << zone_lines[0] << endl;
                for (size_t i = 1; i < zone_lines.size(); i++)
                    os << " | " << zone_lines[i] << endl;
                
            }
        }
        
        BaseRecord* record = *iterator;
        if(record->Type() != "CZONE")
            os << " \\- " << *record << endl;
        else
        {
            CZone* zone = dynamic_cast<CZone*>(record);
            vector<string> zone_lines = zone->ExportLines();
            os << " \\- " << zone_lines[0] << endl;
                for (size_t i = 1; i < zone_lines.size(); i++)
                    os << "   " << zone_lines[i] << endl;
        }
    }

private:
    unsigned long long RecordRawCount;
    set<RecordCluster*, RecordClusterComparator> Clusters;
    set<BaseRecord*, RecordIdComparator> Records;
};

#ifndef __PROGTEST__
int main ( void )
{

return 0;
}
#endif /* __PROGTEST__ */
