#include <iostream>
#include <memory>
#include <math.h>

using namespace std;

template<typename T>
class IBelongsTo
{
public:
    IBelongsTo()
    {
        belongs_to = nullptr;
    }

    shared_ptr<T> belongs_to;
}

template <typename T>
class BinomialNode
{
public:
    BinomialNode(T _data)
    {
        data = _data;
        degree = 0;
        parent = nullptr;
        child = nullptr;
        next = nullptr;
        previous = nullptr;
    }

    T data;
    uint32_t degree;
    shared_ptr<BinomialNode> parent;
    shared_ptr<BinomialNode> child;
    shared_ptr<BinomialNode> next;
    shared_ptr<BinomialNode> previous;
};

template <typename T, typename Cmp = less<T>>
class BinomialHeap
{
private:
    void make_siblings(shared_ptr<BinomialNode<T>> left, shared_ptr<BinomialNode<T>> right)
    {
        if(left->next != nullptr)
            left->next->previous = nullptr;
        left->next = right;

        if(right->previous != nullptr)
            right->previous->next = nullptr;
        right->previous = left;
    }

    void make_parent(shared_ptr<BinomialNode<T>>& left, shared_ptr<BinomialNode<T>>& right)
    {
        if(right->parent != nullptr)
        {
            right->parent->child = right->next;
            if(right->next != nullptr)
                right->next->previous = nullptr;
        }

        if(right->next != nullptr && right->previous != nullptr)
            make_siblings(right->previous, right->next);
        else if(right->next != nullptr)
            right->next->previous = nullptr;
        else if(right->previous != nullptr)
            right->previous->next = nullptr;

        right->previous = nullptr;
        right->next = nullptr;

        if(left->child != nullptr)
            make_siblings(right, left->child);
        
        left->child = right;
        left->child->parent = left;
        left->degree++;
    }

public:
    shared_ptr<BinomialNode<T>> root;
    shared_ptr<BinomialNode<T>> to_extract;
    Cmp comparator;
    T extract_value;

    BinomialHeap(T extract)
    {
        root = nullptr;
        extract_value = extract;
    }

    BinomialHeap(const BinomialHeap<T, Cmp>& other)
    {
        root = other.root;
        extract_value = other.extract_value;
        to_extract = other.to_extract;
    }

    BinomialHeap<T, Cmp>& operator=(const BinomialHeap<T, Cmp>& other)
    {
        root = other.root;
        extract_value = other.extract_value;
        to_extract = other.to_extract;
        return *this;
    }

    void merge_heap(BinomialHeap<T, Cmp> other)
    {
        if(root == nullptr)
        {
            root = other.root;
            return;
        }
        shared_ptr<BinomialNode<T>> traverseA = root;
        shared_ptr<BinomialNode<T>> traverseB = other.root;
        shared_ptr<BinomialNode<T>> traverseC = nullptr;
        shared_ptr<BinomialNode<T>> result = nullptr;

        if(traverseA->degree <= traverseB->degree)
        {
            traverseC = traverseA;
            traverseA = traverseA->next;
        }
        else
        {
            traverseC = traverseB;
            traverseB = traverseB->next;
        }
        result = traverseC;

        while(traverseA != nullptr && traverseB != nullptr)
        {
            if(traverseA->degree <= traverseB->degree)
            {
                make_siblings(traverseC, traverseA);
                traverseA = traverseA->next;
            }
            else
            {
                make_siblings(traverseC, traverseB);
                traverseB = traverseB->next;
            }
            traverseC = traverseC->next;
        }

        while(traverseA != nullptr)
        {
            make_siblings(traverseC, traverseA);
            traverseA = traverseA->next;
            traverseC = traverseC->next;
        }
        while(traverseB != nullptr)
        {
            make_siblings(traverseC, traverseB);
            traverseB = traverseB->next;
            traverseC = traverseC->next;
        }

        traverseC = result;
        shared_ptr<BinomialNode<T>> previous = nullptr;
        shared_ptr<BinomialNode<T>> next = traverseC->next;
        while(next != nullptr)
        {
            if(traverseC->degree != next->degree || (next->next != nullptr && traverseC->degree == next->next->degree))
            {
                previous = traverseC;
                traverseC = next;
            }
            else
            {
                if(comparator(traverseC->data, next->data))
                    make_parent(traverseC, next);
                else
                {
                    if(previous == nullptr)
                        result = next;

                    make_parent(next, traverseC);
                    traverseC = next;
                }
            }
            next = traverseC->next;
        }
        root = result;
        find_min();
    }

    shared_ptr<BinomialNode<T>> insert(T data)
    {
        shared_ptr<BinomialNode<T>> to_insert = make_shared<BinomialNode<T>>(data);
        if(root != nullptr)
        {
            BinomialHeap<T, Cmp> to_merge(extract_value);
            to_merge.root = to_insert;
            merge_heap(to_merge);
        }
        else
            root = to_insert;

        if(to_extract == nullptr || comparator(to_insert->data, to_extract->data))
            to_extract = to_insert;

        return to_insert;
    }

    void find_min()
    {
        to_extract = nullptr;
        if(root == nullptr)
            return;

        shared_ptr<BinomialNode<T>> to_compare = root;
        while(to_compare != nullptr)
        {
            if(to_extract == nullptr || comparator(to_compare->data, to_extract->data))
                to_extract = to_compare;
            to_compare = to_compare->next;
        }
    }

    shared_ptr<BinomialNode<T>> extract()
    {
        if(to_extract == nullptr)
            return nullptr;
        else
        {
            if(to_extract->previous != nullptr && to_extract->next != nullptr)
                make_siblings(to_extract->previous, to_extract->next);
            else if(to_extract->previous != nullptr)
                to_extract->previous->next = nullptr;
            else if(to_extract->next != nullptr)
            {
                to_extract->next->previous = nullptr;
                root = to_extract->next;
            }
            else
                root = nullptr;

            shared_ptr<BinomialNode<T>> temp = nullptr;
            shared_ptr<BinomialNode<T>> child_traverse = to_extract->child;
            while(child_traverse != nullptr)
            {
                child_traverse->parent = nullptr;

                temp = child_traverse->previous;
                child_traverse->previous = child_traverse->next;
                child_traverse->next = temp;
                if(child_traverse->previous == nullptr)
                    break;
                child_traverse = child_traverse->previous;
            }
            child_traverse->previous = nullptr;
            BinomialHeap<T, Cmp> to_merge(extract_value);
            to_merge.root = child_traverse;
            merge_heap(to_merge);
            shared_ptr<BinomialNode<T>> min = to_extract;
            find_min();
            return min;
        }
    }

    shared_ptr<BinomialNode<T>> find_node(shared_ptr<BinomialNode<T>> current, T value)
    {
        if(current == nullptr)
            return nullptr;

        if(comparator(current->data, value) && comparator(value, current->data))
                return current;

        shared_ptr<BinomialNode<T>> depth = find_node(current->child, value);
        if(depth != nullptr)
            return depth;

        return find_node(current->next, value);
    }

    shared_ptr<BinomialNode<T>> change_value(T old_value, T new_value)
    {
        shared_ptr<BinomialNode<T>> to_change = find_node(root, old_value);
        if(to_change == nullptr)
            return nullptr;

        to_change->data = new_value;
        shared_ptr<BinomialNode<T>> parent = to_change->parent;
        while(parent != nullptr && comparator(to_change->data, parent->data))
        {
            swap(to_change->data, parent->data);
            to_change = parent;
            parent = parent->parent;
        }
        find_min();
        return to_change;
    }

    shared_ptr<BinomialNode<T>> delete_value(T value)
    {
        change_value(value, extract_value);
        return extract();
    }

    shared_ptr<BinomialNode<T>> delete_node(shared_ptr<BinomialNode<T>> node)
    {
        shared_ptr<BinomialNode<T>> to_change = node;
        to_change->data = extract_value;
        shared_ptr<BinomialNode<T>> parent = to_change->parent;
        while(parent != nullptr && comparator(to_change->data, parent->data))
        {
            swap(to_change->data, parent->data);
            to_change = parent;
            parent = parent->parent;
        }
        to_extract = to_change;
        return extract();
    }

    shared_ptr<BinomialNode<T>> get_min()
    {
        return to_extract;
    }

};

class IntCompare
{
public:
    bool operator()(const int& left, const int& right) const 
    {
        return left > right;
    }
};

class Party;
class Coalition;

class Politician
{
public:
    Politician()
    {
        id = 0;
        party = nullptr;
        popularity = UINT32_MAX;
        gender = 0;
        order = 0;
        name = "";
        binomial_pointer = nullptr;
    }

    Politician(shared_ptr<Party> _party, uint32_t _id, const string & _name, uint32_t _popularity, uint8_t _gender)
    {
        id = _id;
        party = _party;
        popularity = _popularity;
        gender = _gender;
        order = 0;
        name = _name;
        binomial_pointer = nullptr;
    }

    void dispose()
    {
        party = nullptr;
        binomial_pointer = nullptr;
    }

    uint32_t id;
    shared_ptr<Party> party;
    shared_ptr<BinomialNode<shared_ptr<Politician>>> binomial_pointer;
    uint32_t popularity;
    uint32_t order;
    uint8_t gender;
    string name;
};

class PoliticianPopularityComparator
{
public:
    bool operator()(shared_ptr<Politician>& left, shared_ptr<Politician>& right)
    {
        if(left->popularity == right->popularity)
            return left->order < right->order;
        return left->popularity > right->popularity;
    }
};

class Party
{
public:
    Party() : politicians(BinomialHeap<shared_ptr<Politician>, PoliticianPopularityComparator>(make_shared<Politician>()))
    {
        id = 0;
        coalition = nullptr;
        binomial_pointer = nullptr;
        avl_pointer = nullptr;
    }

    Party(uint32_t _id, BinomialHeap<shared_ptr<Politician>, PoliticianPopularityComparator> _politicians)
    : politicians(_politicians)
    {
        id = _id;
        coalition = nullptr;
        binomial_pointer = nullptr;
        avl_pointer = nullptr;
        politicians = _politicians;
    }

    void dispose()
    {
        coalition = nullptr;
        binomial_pointer = nullptr;
        avl_pointer = nullptr;
    }

    uint32_t id;
    shared_ptr<Coalition> coalition;
    shared_ptr<BinomialNode<shared_ptr<Party>>> binomial_pointer;
    BinomialHeap<shared_ptr<Politician>, PoliticianPopularityComparator> politicians;
};

class Coalition
{
public:
    Coalition(uint32_t _id, BinomialHeap<shared_ptr<Party>, PartyIdComparator> _parties) : parties(_parties)
    {
        id = _id;
    }
    uint32_t id;
    BinomialHeap<shared_ptr<Party>, PartyIdComparator> parties;
};

class CoalitionIdComparator
{
public:
    bool operator()(const shared_ptr<Coalition>& left, const shared_ptr<Coalition>& right)
    {
        return left->id < right->id;
    }
};

class CPulitzer {
private:
    size_t max_parties;
    size_t max_politicians;
    size_t parties;
    size_t politicians;

    shared_ptr<Politician>* politicians;
    shared_ptr<Party>* parties;
    shared_ptr<Coalition> coalitions;

    bool insert_party(shared_ptr<Party> party)
    {
        
    }

public:

    CPulitzer (size_t N, size_t P)
    {
        max_parties = N;
        max_politicians = P;
        parties = 0;
        politicians = 0;
    }
    
    bool register_politician ( uint32_t id_party, uint32_t id_politician, const string & name, uint32_t popularity, uint8_t gender )
    {
        
    }
    
    bool politician_name ( uint32_t id_politician, string & name ) const; 
    
    bool politician_gender ( uint32_t id_politician, uint8_t & gender ) const;
    
    bool politician_popularity ( uint32_t id_politician, uint32_t & popularity ) const;
    
    bool deregister_politician ( uint32_t id_politician );
    
    bool party_leader ( uint32_t id_party, uint32_t & id_leader ) const;
    
    bool change_popularity ( uint32_t id_politician, uint32_t popularity );
    
    bool sack_leader ( uint32_t id_party );
    
    bool merge_parties ( uint32_t dest_party, uint32_t src_party );
    
    bool create_coalition ( uint32_t id_party1, uint32_t id_party2 );
    
    bool leave_coalition ( uint32_t id_party );
    
    bool coalition_leader ( uint32_t id_party, uint32_t & id_leader ) const;
    
    bool scandal_occured ( uint32_t id_party );
        
};

int main (void)
{
    CPulitzer bot( 3, 10 );
    bool result = false;
    result = bot.register_politician( 1, 5, "VK", 1000, 77 ); // true
    result = bot.register_politician( 2, 4, "MZ", 1000, 77 ); // true
    result = bot.register_politician( 2, 7, "VS", 500, 77 ); // true
    return 0;
}