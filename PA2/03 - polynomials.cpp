#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cmath>
#include <cfloat>
#include <cassert>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#if defined ( __cplusplus ) && __cplusplus > 199711L /* C++ 11 */
#include <memory>
#endif /* C++ 11 */
using namespace std;
#endif /* __PROGTEST__ */

class CPolynomial
{
public:
    CPolynomial()
    {
        Members.push_back(0.0);
    }

    CPolynomial(const CPolynomial & polynom)
    {
        Members.clear();
        Members.shrink_to_fit();
        for (size_t i = 0; i < polynom.Members.size(); i++)
            Members.push_back(polynom.Members[i]);
    }

    ~CPolynomial() = default;

    bool operator==(const CPolynomial& right) const
    {
        if(Degree() != right.Degree())
            return false;
        
        for (unsigned int i = 0; i <= Degree(); i++)
        {
            if(!compareDoubles(Members[i], right.Members[i]))
                return false;
        }
        return true;
    }

    bool operator!=(const CPolynomial& right) const
    {
        return !(*this == right);
    }

    CPolynomial& operator=(const CPolynomial & polynom)
    {
        if(*this != polynom)
        {
            Members.clear();
            Members.shrink_to_fit();

            for (size_t i = 0; i < polynom.Members.size(); i++)
                Members.push_back(polynom.Members[i]);
        }

        return *this;
    }

    CPolynomial operator+ (const CPolynomial& right) const
    {
        CPolynomial new_pol;
        unsigned int ldegree = Degree();
        unsigned int rdegree = right.Degree();
        unsigned int degree = max(ldegree, rdegree);
        
        unsigned int add = degree - (new_pol.Members.size() - 1);
        for (unsigned int i = 0; i < add; i++)
            new_pol.Members.push_back(0.0);

        for (unsigned int i = 0; i <= degree; i++)
        {
            double to_add = 0.0;
            if(ldegree >= i)
                to_add += Members[i];
            if(rdegree >= i)
                to_add += right.Members[i];

            new_pol.Members[i] = to_add;
        }

        return new_pol;
    }

    CPolynomial operator- (const CPolynomial& right) const
    {
        CPolynomial new_pol;
        unsigned int ldegree = Degree();
        unsigned int rdegree = right.Degree();
        unsigned int degree = max(ldegree, rdegree);
        
        unsigned int add = degree - (new_pol.Members.size() - 1);
        for (unsigned int i = 0; i < add; i++)
            new_pol.Members.push_back(0.0);

        for (unsigned int i = 0; i <= degree; i++)
        {
            double to_add = 0.0;
            if(ldegree >= i)
                to_add += Members[i];
            if(rdegree >= i)
                to_add -= right.Members[i];

            new_pol.Members[i] = to_add;
        }

        return new_pol;
    }

    CPolynomial operator* (const double & val) const
    {
        CPolynomial temp;

        temp = *this;

        for (size_t i = 0; i < Members.size(); i++)
            temp.Members[i] *= val;

        return temp;
    }

    CPolynomial operator* (const CPolynomial& right) const
    {
        CPolynomial temp;

        for (size_t i = 0; i < Members.size(); i++)
            for (size_t j = 0; j < right.Members.size(); j++)
                temp[i + j] += Members[i] * right.Members[j];
        
        return temp;
    }

    double& operator [](const unsigned int& exponent)
    {
        int to_add = exponent - (Members.size() - 1);
        for (int i = 0; i < to_add; i++)
            Members.push_back(0.0);
        
        return Members[exponent];
    }

    double operator [](const unsigned int& exponent) const
    {
        return Members[exponent];
    }

    double operator() (const double& x) const
    {
        double value = 0;
        for (size_t i = 0; i < Members.size(); i++)
        {
            value += pow(x, (double)i) * Members[i];
        }
        return value;
    }
    
    unsigned int Degree () const
    {
        for (size_t i = Members.size() - 1; i > 0; i--)
        {
            if(!compareDoubles(Members[i], 0.0))
               return i; 
        }

        return 0;
    }
    
    friend std::ostream& operator<<(std::ostream& os, const CPolynomial& obj)
    {
        bool did_start = false;
        
        for (int i = (int)obj.Members.size() - 1; i >= 0; i--)
        {
            if(obj.compareDoubles(0.0, obj.Members[i]))
            {
                if(i == 0 && !did_start)
                {
                    os << '0';
                    return os;
                }
                continue;
            }


            if(!did_start)
            {
                if(obj.compareDoubles(1.0, obj.Members[i]))
                {
                    if(i != 0)
                        os << "x^" << i;
                    else
                        os << "1";
                }
                else if(obj.compareDoubles(-1.0, obj.Members[i]))
                {
                    if(i != 0)
                        os << "- x^"<< i;
                    else
                        os << "- 1";
                }
                else if(obj.Members[i] > 0.0)
                {
                    os << abs(obj.Members[i]);
                    if(i != 0)
                        os << "*x^" << i;  
                }
                else
                {
                    os << "- " << abs(obj.Members[i]);
                    if(i != 0)
                        os << "*x^" << i;   
                }

                did_start = true;
                continue;
            }

            if(obj.compareDoubles(1.0, obj.Members[i]))
                {
                    if(i != 0)
                        os << " + x^" << i;
                    else
                        os << " + 1";
                }
                else if(obj.compareDoubles(-1.0, obj.Members[i]))
                {
                    if(i != 0)
                        os << " - x^"<< i;
                    else
                        os << " - 1";
                }
                else if(obj.Members[i] > 0.0)
                {
                    os << " + " << abs(obj.Members[i]);
                    if(i != 0)
                        os << "*x^" << i;  
                }
                else
                {
                    os << " - " << abs(obj.Members[i]);
                    if(i != 0)
                        os << "*x^" << i;   
                }
        }
        
        return os;
    }


private:
    vector<double> Members;

    bool compareDoubles( const double a, const double b ) const
    {
        return fabs(a - b) < DBL_EPSILON * 10e4;
    }
};

#ifndef __PROGTEST__
bool smallDiff( double a, double b )
{
    return fabs(a - b) < DBL_EPSILON * 10e4;
}

bool dumpMatch( const CPolynomial & x, const vector<double> & ref )
{
    for (size_t i = 0; i < ref.size(); i++)
    {
        if(!smallDiff(x[i], ref[i]))
            return false;
    }
    return true;
}

int main ( void )
{
    CPolynomial a, b, c;
    ostringstream out;
    string test;

    a[0] = -10;
    a[1] = 3.5;
    a[3] = 1;
    assert ( smallDiff ( a ( 2 ), 5 ) );
    out . str ("");
    out << a;
    test = out.str();
    assert ( out . str () == "x^3 + 3.5*x^1 - 10" );
    a = a * -2;
    assert ( a . Degree () == 3 && dumpMatch ( a, vector<double>{ 20.0, -7.0, -0.0, -2.0 } ) );

    out . str ("");
    out << a;
    test = out.str();
    assert ( out . str () == "- 2*x^3 - 7*x^1 + 20" );
    out . str ("");
    out << b;
    test = out.str();
    assert ( out . str () == "0" );
    b[5] = -1;
    out . str ("");
    out << b;
    test = out.str();
    assert ( out . str () == "- x^5" );
    c = a + b;
    assert ( c . Degree () == 5 && dumpMatch ( c, vector<double>{ 20.0, -7.0, 0.0, -2.0, 0.0, -1.0 } ) );

    out . str ("");
    out << c;
    assert ( out . str () == "- x^5 - 2*x^3 - 7*x^1 + 20" );
    c = a - b;
    assert ( c . Degree () == 5 && dumpMatch ( c, vector<double>{ 20.0, -7.0, -0.0, -2.0, -0.0, 1.0 } ) );

    out . str ("");
    out << c;
    assert ( out . str () == "x^5 - 2*x^3 - 7*x^1 + 20" );
    c = a * b;
    assert ( c . Degree () == 8 && dumpMatch ( c, vector<double>{ -0.0, -0.0, 0.0, -0.0, 0.0, -20.0, 7.0, -0.0, 2.0 } ) );

    out . str ("");
    out << c;
    assert ( out . str () == "2*x^8 + 7*x^6 - 20*x^5" );
    assert ( a != b );
    b[5] = 0;
    assert ( !(a == b) );
    a = a * 0;
    assert ( a . Degree () == 0 && dumpMatch ( a, vector<double>{ 0.0 } ) );

    assert ( a == b );
    return 0;
}
#endif /* __PROGTEST__ */
