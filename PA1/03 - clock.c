#include <stdio.h>
#include <assert.h>

#ifndef __PROGTEST__
#include <stdio.h>
#include <assert.h>
#endif /* __PROGTEST__ */

long long int GetDaysFromRDEpoch(long long int year, long long int month, long long int day) 
{ 
  if (month < 3)
  {
      year--; 
      month += 12;
  }

  long long int base = 365 * year;
  long long int leap_days = year / 4 - year / 100 + year / 400 - year / 4000;
  long long int feb_shift = (153 * month - 457) / 5 + day - 306;

  return base + leap_days + feb_shift;
}

long long int GetCuckooFromDayStart(long long int hour, long long int minutes)
{
  long long int cuckoo = 0;
  if(minutes >= 30)
  {
    cuckoo += hour + 1;
  }
  else
  {
    cuckoo += hour;
  }
  if(hour > 12)
  {
    cuckoo += 78;
    hour -= 12;
  }
  cuckoo += hour * (0.5 + (0.5 * hour));
  return cuckoo;
}

long long int GetCuckooToDayEnd(long long int hour, long long int minutes)
{
  return 180 - GetCuckooFromDayStart(hour, minutes);
}

int isLeap(long long int year)
{
  return ((year % 4 == 0 && (year % 100 != 0)) || (year % 400 == 0 && year % 4000 != 0));
}

int CheckDate(long long int y1, long long int m1, long long int d1, long long int h1, long long int i1)
{
  if(!(y1 >= 1600 && m1 > 0 && m1 < 13 && h1 > -1 && h1 < 24 && i1 > - 1 && i1 < 60 && d1 > 0 && d1 < 32))
  {
    return 0;
  }
  if(m1 == 2)
  {
    if(isLeap(y1))
      return d1 <= 29;
    else
     return d1 <= 28;
  }
  if (m1 == 4 || m1 == 6 || m1 == 9 || m1 == 11) 
    return (d1 <= 30); 

  return 1;
}

int cuckooClock ( int y1, int m1, int d1, int h1, int i1,
                  int y2, int m2, int d2, int h2, int i2, long long int * cuckoo )
{

  if(!CheckDate(y1,m1,d1,h1,i1) || !CheckDate(y2,m2,d2,h2,i2))
  {
    return 0;
  }
  long long int interval_end = GetDaysFromRDEpoch(y2, m2, d2 - 1);
  long long int interval_start = GetDaysFromRDEpoch(y1, m1, d1 + 1);
  long long int day_difference = interval_end - interval_start + 1;

  if(day_difference < -1)
  {
    return 0;  
  }
  if(day_difference == -1)
  {
    if(h1 > h2)
     return 0;
    if(h1 == h2 && (i1 > i2))
      return 0;  
    }

  *cuckoo = 0;
  long long int cuckoo_per_day = 180;
  if(m1 == 12 && d1 == 31)
  {
    y1++;
    m1 = 1;
    d1 = 1;
  }
  if(m2 == 1 && d2 == 1)
  {
    y2--;
    m1 = 12;
    d1 = 31;
  }
  if(day_difference > 0)
  {
    *cuckoo += day_difference * cuckoo_per_day;
    long long int start = GetCuckooFromDayStart(h2,i2);
    long long int end = GetCuckooToDayEnd(h1, i1);
    *cuckoo += start;
    *cuckoo += end;
  }
  else if(day_difference == 0)
  {
    *cuckoo += GetCuckooToDayEnd(h1, i1);
    *cuckoo += GetCuckooFromDayStart(h2, i2);
  }
  else
  {
    long long int start = GetCuckooFromDayStart(h1,i1);
    long long int end = GetCuckooToDayEnd(h2, i2);
    *cuckoo = 180 - (start + end);
  }

  if((h1 == 0 && i1 == 0) || (h1 == 12 && i1 == 0))
    *cuckoo += 12;
  else if(i1 == 0)
  {
    if(h1 > 12)
      *cuckoo += h1 - 12;
    else
    {
      *cuckoo += h1;
    }
    
  }
  else if(i1 == 30)
    *cuckoo += 1;

  printf("%lld\n", *cuckoo);

  if(*cuckoo < 0)
    return 0;

  return 1;
    
}

#ifndef __PROGTEST__
int main ( int argc, char * argv [] )
{
  long long int cuckoo;
  assert(cuckooClock ( 2040, 10, 3, 23, 30, 2040, 10, 3, 23, 15, &cuckoo ) == 0);
  assert(cuckooClock (2007, 9, 5, 23, 30, 2007, 9, 5, 23, 45, &cuckoo) == 1 && cuckoo == 1);
  assert ( cuckooClock ( 2034, 7, 1, 23, 0, 2034, 7, 1, 23, 45, &cuckoo )== 1 && cuckoo == 12);
  assert ( cuckooClock ( 2074, 3, 26, 2, 0, 2074, 3, 26, 2, 30, &cuckoo )== 1 && cuckoo == 3);
  assert(cuckooClock(2100, 11, 30, 0, 0, 2100, 12, 29, 0, 0, &cuckoo) == 1 && cuckoo == 5232);
  assert(cuckooClock(1852, 1, -9, 0, 0, 2400, 1, 1, 0, 0, &cuckoo) == 0);
    assert ( cuckooClock ( 2020, 10,  1, 13, 15,
                            2020, 10,  1, 18, 45, &cuckoo ) == 1 && cuckoo == 26 );
    assert ( cuckooClock ( 2020, 10,  1, 13, 15,
                            2020, 10,  2, 11, 20, &cuckoo ) == 1 && cuckoo == 165 );
    assert ( cuckooClock ( 2020,  1,  1, 13, 15,
                            2020, 10,  5, 11, 20, &cuckoo ) == 1 && cuckoo == 50025 );
    assert ( cuckooClock ( 2019,  1,  1, 13, 15,
                            2019, 10,  5, 11, 20, &cuckoo ) == 1 && cuckoo == 49845 );
    assert ( cuckooClock ( 1900,  1,  1, 13, 15,
                            1900, 10,  5, 11, 20, &cuckoo ) == 1 && cuckoo == 49845 );
    assert ( cuckooClock ( 2020, 10,  1,  0,  0,
                            2020, 10,  1, 12,  0, &cuckoo ) == 1 && cuckoo == 102 );
    assert ( cuckooClock ( 2020, 10,  1,  0, 15,
                            2020, 10,  1,  0, 25, &cuckoo ) == 1 && cuckoo == 0 );
    assert ( cuckooClock ( 2020, 10,  1, 12,  0,
                            2020, 10,  1, 12,  0, &cuckoo ) == 1 && cuckoo == 12 );
    assert ( cuckooClock ( 2020, 11,  1, 12,  0,
                            2020, 10,  1, 12,  0, &cuckoo ) == 0 );
    assert ( cuckooClock ( 2020, 10, 32, 12,  0,
                            2020, 11, 10, 12,  0, &cuckoo ) == 0 );
    assert ( cuckooClock ( 2100,  2, 29, 12,  0,
                            2100,  2, 29, 12,  0, &cuckoo ) == 0 );
    assert ( cuckooClock ( 2400,  2, 29, 12,  0,
                            2400,  2, 29, 12,  0, &cuckoo ) == 1 && cuckoo == 12 );
    assert(cuckooClock(2100, 11, 30, 0, 0, 2100, 12, 29, 0, 0, &cuckoo) == 1 && cuckoo == 5232);
    assert( cuckooClock ( 2100, 12, 11, 0, 0, 2100, 12, 31, 0, 0, &cuckoo ) == 1 && cuckoo == 3612);
    assert( cuckooClock ( 2020, 10, 32, 12, 0, 2020, 11, 10, 12, 0, &cuckoo ) == 0);
    assert(cuckooClock(2181, 5, 18, 9, 34, 2062080312, 7, 22, 21, 16, &cuckoo) == 1 && cuckoo == 135568450135169);
    assert(cuckooClock(1924, 12, 21, 1, 2, 115041459, 8, 17, 21, 30, &cuckoo) == 1 && cuckoo == 7563113725823);
    assert(cuckooClock(1965, 7, 18, 23, 11, 1588631495, 2, 10, 8, 53, &cuckoo) == 1 && cuckoo == 104442232283158);
    assert(cuckooClock(1711, 10, 25, 0, 23, 457691589, 5, 2, 7, 47, &cuckoo) == 1 && cuckoo == 30090182520276);
    assert(cuckooClock(1792, 4, 31, 13, 27, 355871257, 9, 30, 18, 32, &cuckoo) == 0 && cuckoo == 30090182520276);
    assert(cuckooClock(1988, 9, 12, 23, 18, 741828622, 11, 29, 5, 44, &cuckoo) == 1 && cuckoo == 48770357217874);
    assert(cuckooClock(1664, 3, 17, 0, 49, 1294704329, 3, 20, 23, 26, &cuckoo) == 1 && cuckoo == 85118420600626);
  return 0;
}
#endif /* __PROGTEST__ */
