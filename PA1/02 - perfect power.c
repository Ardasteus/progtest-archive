#include <stdio.h>
#include <math.h>

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

int CustomPow(int x, int y)
{
    int res = 1.0;
    for (int j = 0; j < (int)y; j++)
    {
        res *= x;
    }
    return res;
}

int CheckPerfectPower(int n)
{
    if(n == 1.0)
        return 1;

    int border = sqrt(n);

    for (int i = 2; i <= border; i++)
    {
        int y = 2;
        int p = CustomPow(i,y);

        while (p<=n && p>0)
        {
            if(p ==n)
                return 1;

            y++;
            p = CustomPow(i,y);
        }
        
    }
    return 0;
}

int CountAllPower(int start, int end)
{
    int result = 0;
    int origin = 2;
    int rootEnd = sqrt(end);
    int exponentEnd = 0;
    int exponentStart = 0;
    while(origin <= rootEnd)
    {
        exponentEnd = MAX(floor((log(end) / log(origin))), 2);
        exponentStart = MAX(ceil((log(start) / log(origin))), 2);
        if(!CheckPerfectPower(origin))
        {
            for (int i = exponentEnd; i >= exponentStart; i--)
            {
                int res = CustomPow(origin, i);
                if(res <= end && res >= start)
                {
                    result += 1;
                }
            }
        }
        origin += 1;
    }

    if(start == 1)
    {
        return result;
    }
    else
    {
        return result - 1;
    }
}

int main (void)
{
    int inp1 = 0.0;
    int inp2 = 0.0;
    char input[1024];
    printf("Intervaly:\n");
    while(1)
    {
        if(fgets(input, sizeof(input),stdin) == NULL)
        {
            return 0;
        }
        if(sscanf(input,"%d %d",  &inp1, &inp2) != 2 || inp1 < 1 || inp2 < 1 || inp1 > inp2)
        {
            printf("Nespravny vstup.\n");
            return 0;
        }
        int x = CountAllPower(inp1, inp2);
        printf("<%d;%d> -> %d\n", inp1, inp2, (inp2 - inp1) - x);
    }
    return 0;
}