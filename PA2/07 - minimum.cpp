#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cctype>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <set>
#include <list>
#include <forward_list>
#include <map>
#include <vector>
#include <string>
#include <algorithm>
#include <functional>
#include <memory>
#include <stdexcept>
using namespace std;
#endif /* __PROGTEST__ */


template<typename T_, typename Cmp_ = less<T_>>
class CIntervalMin 
{
public:
    using const_iterator = typename vector<T_>::const_iterator;

    CIntervalMin() { CreateTable(); };

    CIntervalMin(const Cmp_& comparator) : Comparator(comparator){ CreateTable(); }

    CIntervalMin(const_iterator start, const_iterator end) : values(start, end){ CreateTable(); }

    CIntervalMin(const_iterator start, const_iterator end, const Cmp_& comparator) : values(start, end), Comparator(comparator) { CreateTable(); }

    void push_back(const T_& value)
    {
        values.push_back(value);
        AddToTable(value);
    }

    void pop_back()
    {
        values.pop_back();
        RemoveFromTable();
    }

    const_iterator begin() const
    {
        return values.cbegin();
    }

    const_iterator end() const
    {
        return values.cend();
    }

    T_ min(const_iterator st, const_iterator en) const
    {
        if(en <= st)
            throw std::invalid_argument("Invalid argument");

        int start = st - values.begin();
        int end = en - values.begin() - 1; 

        int position = (int)log2(end - start + 1);
        int offset = end - (1 << position) + 1;
        if(Comparator(Table[position][start], Table[position][offset]))
            return Table[position][start];
        else if(Comparator(Table[position][offset], Table[position][start]))
            return Table[position][offset];
        else
            return Table[position][start];
    }

    size_t size() const
    {
        return values.size();
    }

private:

    void CreateTable() const
    {
        int col = (int)values.size();
        int row = ceil(log2(col)) + 1;
        if(col == 0)
            row = 1;
        
        Rows = row;
        Columns = col;

        Table.clear();
        for(int i = 0; i < row; i++)
        { 
            vector<T_> new_row;
            Table.push_back(new_row);
        }

        for (int i = 0; i < col; i++) 
            Table[0].push_back(values[i]);

        for (int i = 1; i <= row; i++) 
            for (int j = 0; j + (1<<i) <= col; j++)
            {
                if(Comparator(Table[i-1][j], Table[i-1][j + (1<<(i-1))]))
                    Table[i].push_back(Table[i-1][j]);
                else if(Comparator(Table[i-1][j + (1<<(i-1))], Table[i-1][j]))
                    Table[i].push_back(Table[i-1][j + (1<<(i-1))]);
                else
                    Table[i].push_back(Table[i-1][j]);
            }
    }

    void RemoveFromTable() const
    {
        for(int i = 0; i < Rows; i++)
        { 
            if(!Table[i].empty())
                Table[i].pop_back();
        }

        if(Columns > 0)
            Columns--;

        if((Rows - 1) >= 0 && Table[Rows - 1].empty())
        {
            Table.pop_back();
            Rows--;
        }
    }

    void AddToTable(const T_& value) const
    {
        Table[0].push_back(value);
        int col = (int)values.size();
        int row = ceil(log2(col)) + 1;
        if(row != Rows)
        {
            vector<T_> new_row;
            Table.push_back(new_row);
        }

        Rows = row;
        Columns = col;

        T_ local_min = value;

        for(int i = 1; i < Rows; i++)
        { 
            int index = (int)Table[i].size() - 1;
            T_ up = Table[i - 1][index + 1];
            if(Table[i].empty())
            {
                int prev_size = Table[0].size();
                int power = (int)pow(2, i);
                if(prev_size < power)
                    continue;
                if(CompareEqual(up, local_min))
                    local_min = up;

                Table[i].push_back(local_min);
                continue;
            }
            T_ left_top = Table[i - 1][index];
            T_ left = Table[i][index];
            if(Comparator(local_min, left))
            {
                Table[i].push_back(local_min);
            }
            else
            {
                if(!Equal(left, left_top))
                    local_min = left;
                else if(CompareEqual(up, local_min))
                    local_min = up;  

                Table[i].push_back(local_min);
            }
        }
    }

    bool Equal(const T_& left, const T_& right) const
    {
        if(Comparator(left, right))
            return false;
        else if(Comparator(right, left))
            return false;
        else
            return true;
    } 

    bool CompareEqual(const T_& left, const T_& right) const
    {
        if(Comparator(left, right))
            return true;
        else if(Comparator(right, left))
            return false;
        else
            return true;
    }

    mutable int Columns = 0;
    mutable int Rows = 0;

    mutable vector<vector<T_>> Table;
    vector<T_> values;
    Cmp_ Comparator;
};



#ifndef __PROGTEST__
//-------------------------------------------------------------------------------------------------
class CStrComparator
{
public:
CStrComparator ( bool byLength = true ) 
: m_ByLength ( byLength ) 
{ 
}
bool       operator () ( const string & a, const string & b ) const 
{ 
return m_ByLength ? a . length () < b . length () : a < b;
}
private:
bool       m_ByLength;  
};
//-------------------------------------------------------------------------------------------------
bool strCaseCmpFn ( const string & a, const string & b )
{
return strcasecmp ( a . c_str (), b . c_str () ) < 0;
}
//-------------------------------------------------------------------------------------------------
int main ( void )
{

vector<string> testwords{ "prog", "iflos", "void", "NULL" };
CIntervalMin<string, CStrComparator> test(testwords.begin(), testwords.end());
string min = test.min(test.begin(), test.end());


CIntervalMin <int> a1;
for ( auto x : initializer_list<int> { 5, 15, 79, 62, -3, 0, 92, 16, 2, -4 } )
a1 . push_back ( x );

assert ( a1 . size () == 10 );

ostringstream oss;
for ( auto x : a1 )
oss << x << ' ';

assert ( oss . str () == "5 15 79 62 -3 0 92 16 2 -4 " );
assert ( a1 . min ( a1 . begin (), a1 . end () ) == -4 );
assert ( a1 . min ( a1 . begin () + 2, a1 . begin () + 3 ) == 79 );
assert ( a1 . min ( a1 . begin () + 2, a1 . begin () + 9 ) == -3 );

try
{
a1 . min ( a1 . begin (), a1 . begin () );
assert ( "Missing an exception" == nullptr );
}
catch ( const invalid_argument & e )
{
}
catch ( ... )
{
assert ( "Invalid exception" == nullptr );
}

a1 . pop_back ();
assert ( a1 . size () == 9 );
a1 . push_back ( 42 );

assert ( a1 . min ( a1 . begin (), a1 . end () ) == -3 );

vector<string> words{ "auto", "if", "void", "NULL" };
CIntervalMin <string> a2 ( words . begin (), words . end () );
assert ( a2 . min ( a2 . begin (), a2 . end () ) ==  "NULL" );

CIntervalMin <string, bool(*)(const string &, const string &)> a3 ( words . begin (), words . end (), strCaseCmpFn );
assert ( a3 . min ( a3 . begin (), a3 . end () ) == "auto" );

CIntervalMin <string, CStrComparator> a4 ( words . begin (), words . end () );
assert ( a4 . min ( a4 . begin (), a4 . end () ) == "if" );

CIntervalMin <string, CStrComparator> a5 ( words . begin (), words . end (), CStrComparator ( false ) );
assert ( a5 . min ( a5 . begin (), a5 . end () ) == "NULL" );

CIntervalMin <string, function<bool(const string &, const string &)> > a6 ( [] ( const string & a, const string & b )
{
return a > b;
} );
for ( const auto & w : words )
a6 . push_back ( w );
assert ( a6 . min ( a6 . begin (), a6 . end () ) == "void" );
return 0;
}
#endif /* __PROGTEST__ */
