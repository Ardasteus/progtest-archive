# progtest-archive

Sorts as a repository to store all my work on progtests. The code will usually be the solution to the problem (usually for full or bonus points) or a solution that should work and I just did not have the willpower to debug for a week.
This repository solely exists so that I do not lose more of my school work.

Use these in your progtests at your own risk.