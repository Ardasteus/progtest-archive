#include <stdio.h>
#include <stdlib.h>

int compare(const void* a, const void* b)
{
    int value_a = *(const int*)a;
    int value_b = *(const int*)b;
    return value_a > value_b;
}

long long int GetTriangles(int *input, int size)
{
    qsort(input, size, sizeof(int), compare);

    long long int triangles = 0;

    int previous_i = - 1;
    int substract = 0;
    for (int i = 0; i < size - 2; i++)
    {
        if(input[i] == previous_i)
        {
            previous_i = input[i];
            continue;
        }
        int previous_j = - 1;
        for (int j = i + 1; j < size; j++)
        {
            if(input[j] == previous_j)
            {
                previous_j = input[j];
                continue;
            }
            int k = j + 1;
            int previous_k = -1;
            while (k < size && input[i] + input[j] > input[k])
            {
                if(previous_k == input[k])
                {
                    substract++;
                }
                previous_k = input[k];
                k++;
            }
            if(k > j)
            {
                triangles += k - j - 1;
            }
            
            previous_j = input[j];
        }
        previous_i = input[i];
    }


    return triangles - substract;
}

int main(void)
{
    int capacity = 4;
    int *inputs = (int*)malloc(capacity * sizeof(int));
    int num_of_inputs = 0;
    long long int triangles;
    printf("Delky nosniku:\n");
    while (1)
    {
        int input = 0;
        int scanf_return = scanf("%d ", &input);
        if(scanf_return == 1 && input > 0)
        {
            num_of_inputs++;
            if(num_of_inputs == capacity - 1)
            {
                capacity *= 2;
                inputs = (int*)realloc(inputs, capacity * sizeof(int));
            }
            inputs[num_of_inputs - 1] = input;
        }
        else
        {
            if(scanf_return != EOF)
            {
                printf("Nespravny vstup.\n");
                free(inputs);
                return 0;
            }
            break;
        }
        
    }
    
    if(num_of_inputs < 3 || num_of_inputs > 10000)
    {
        printf("Nespravny vstup.\n");
        free(inputs);
        return 0;
    }

    triangles = GetTriangles(inputs, num_of_inputs);
    printf("Trojuhelniku: %lld\n", triangles);

    free(inputs);

}