#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

const char T9_values[26] = { '2', '2', '2', '3', '3', '3', '4', '4', '4', '5', '5', '5', '6', '6', '6', '7', '7', '7', '7', '8', '8', '8', '9', '9', '9', '9'};

#define END 3
#define UPDATED 2
#define TRUE 1
#define FALSE 0

#ifdef _MSC_VER
#pragma region String
#endif
/**
String structue to hold a dynamic array of characters
*/
typedef struct String
{
    char *characters;
    unsigned long allocated_size;
    unsigned long used_size;
} String;

/**
Create a string instance and preallocates an array of characters with given size

@param unsigned long size - size of the char array
@returns String
*/
String string_create(const unsigned long size)
{
    String string;
    
    string.allocated_size = size;
    string.used_size = 0;
    string.characters = (char*)malloc((size + 1) * sizeof(char));
    string.characters[0] = '\0';

    return string;
}

/**
Create a string instance with a given character array

@param char* characters - character array to be assigned to the string
@returns String
*/
String string_create(char* characters)
{
    String string;
    
    int size = strlen(characters);

    string.allocated_size = size + 1;
    string.used_size = size;
    string.characters = characters;

    return string;
}

/**
Reallocates the char array for given string

@param String* string - string to be modified
@param unsigned long size - size of the new char array
*/
void string_reallocate(String* string, const unsigned long size)
{
    string->allocated_size = size;
    string->characters = (char*)realloc(string->characters, (size + 1) * sizeof(char));
    string->characters[string->allocated_size] = '\0';
}

/**
Appends a character to a string, increasing its size if needed

@param String* string - string to be modified
@param char c - character to be appended
*/
void string_append(String* string, const char c)
{
    if(string->allocated_size <= string->used_size)
        string_reallocate(string, string->allocated_size * 2);
    
    string->characters[string->used_size++] = c;
    string->characters[string->used_size] = '\0';
}

/**
Disposes of all resources used by a given string

@param String* string - string to be disposed
*/
void string_dispose(String* string)
{
    if(string->characters)
        free(string->characters);
}

/**
Converts a string to an integer and returns the result

@param String* string - string to be converted
@return int - resulting conversion of the string
*/
int string_to_int(const String* string)
{
    return atoi(string->characters); 
}

/**
Converts a string to a T9 string and returns the result

@param String* string - string to be converted
@return String - resulting conversion of the string
*/
String string_to_T9(const String* string)
{
    String T9_return = string_create(4);
    for (unsigned long i = 0; i < string->used_size; i++)
    {
        if(isalpha(string->characters[i]))
        {
            char digit = T9_values[tolower(string->characters[i]) - ((int)'a')];
            string_append(&T9_return, digit);
        }
        else if(isdigit(string->characters[i]))
            string_append(&T9_return, string->characters[i]);
        else if(isspace(string->characters[i]))
            string_append(&T9_return, '1');
        else
            string_append(&T9_return, '0');
    }

    return T9_return;
}

/**
Creates a copy of a string

@param String* string - string to be copied
*/
String string_copy(const String* string)
{
    String copy = string_create(string->used_size);

    for (unsigned long i = 0; i < string->used_size; i++)
    {
        string_append(&copy, string->characters[i]);
    }

    return copy;
}

#ifdef _MSC_VER
#pragma endregion String
#endif

#ifdef _MSC_VER
#pragma region Record
#endif

/**
Record struct to hold all information about a record. 
Because there are multiple pointers to one record I also keep a short with the number of pointers to the Record instance.
*/
typedef struct Record
{
    String phone_number;
    String user_name;
    short exists;
    short pointers;
} Record;

/**
Create a record instance using a string representing the phone number and a string representing the username.

@param String number - string representing the phone number
@param String number - string representing the username
@returns Record*
*/
Record* record_create(const String number, const String name)
{
    Record* record = (Record*)malloc(sizeof(Record));

    record->exists = TRUE;
    record->phone_number = number;
    record->user_name = name;
    record->pointers = 0;

    return record;
}

/**
Disposes a given Record instance, if there is only one pointer to it, Otherwise just decerements the number of pointers.

@param Record* record - record to be disposed
*/
void record_dispose(Record* record)
{
    if(record)
    {
        if(record->pointers > 1)
            record->pointers--;
        else
        {
            string_dispose(&record->phone_number);
            string_dispose(&record->user_name);
            free(record);
        }
    }
}

/**
Prints message "FOUND {phone_number} ({user_name})" to standard input.

@param Record* record - record to be printed
*/
void record_print(Record* record)
{
    if(record)
    {
        printf("FOUND ");
        printf(record->phone_number.characters);
        printf(" (");
        printf(record->user_name.characters);
        printf(")");
        printf("\n");
    }
}

#ifdef _MSC_VER
#pragma endregion Record
#endif

#ifdef _MSC_VER
#pragma region RecordList
#endif

/**
RecordList is a structure that holds a dynamic array of Record instances.
*/
typedef struct RecordList
{
    Record** records;
    unsigned long allocated_size;
    unsigned long used_size;
} RecordList;

/**
Creates a RecordList instance and allocates space for Records.

@param unsigned long size - size of the dynamic array
*/
RecordList record_list_create(const unsigned long size)
{
    RecordList record_list;
    
    record_list.allocated_size = size;
    record_list.used_size = 0;
    record_list.records = (Record**)malloc(size * sizeof(Record*));

    return record_list;
}

/**
Reallocates the dynamic array of Records in a given RecordList

@param RecordList* record_list - RecordList to be modifieid
@param unsigned long size - size of the new array
*/
void record_list_reallocate(RecordList* record_list, const unsigned long size)
{
    record_list->allocated_size = size;
    record_list->records = (Record**)realloc(record_list->records, size * sizeof(Record*));
}

/**
Adds a given Record to the array of Records in a given RecordList

@param RecordList* record_list - RecordList to be modifieid
@param Record* record - record to be added
*/
void record_list_append(RecordList* record_list, Record* record)
{
    if(record_list->allocated_size <= record_list->used_size)
        record_list_reallocate(record_list, record_list->allocated_size * 2);

    record->pointers++;
    record_list->records[record_list->used_size++] = record;
}

/**
Releases all resources used by a given RecordList

@param RecordList* record_list - RecordList to be disposed
*/
void record_list_dispose(RecordList* record_list)
{
    for (unsigned long i = 0; i < record_list->used_size; i++)
        if(record_list->records[i])
            record_dispose(record_list->records[i]);

    free(record_list->records);
}

#ifdef _MSC_VER
#pragma endregion RecordList
#endif

#ifdef _MSC_VER
#pragma region RecordResource
#endif

/**
RecordResource is the core of the switchboard system. It holds the identifier string, a phone record and a list of all records which name converts to the identifier in T9.
*/
typedef struct RecordResource
{
    String identifier;
    Record* number_record;
    RecordList user_name_records;
} RecordResource;

/**
Creates an instance of RecordResource.

@param String identifier
*/
RecordResource record_resource_create(const String identifier)
{
    RecordResource record_resource;

    record_resource.identifier = identifier;
    record_resource.number_record = NULL;
    record_resource.user_name_records = record_list_create(2);

    return record_resource;
}

/**
Frees all resources used by the RecordResouse instance

@param RecordResource* resource
*/
void record_resource_dispose(RecordResource* resource)
{
    if(resource)
    {
        string_dispose(&resource->identifier);
        record_dispose(resource->number_record);
        record_list_dispose(&resource->user_name_records);
    }
}

/**
Compares two RecordResources based on their identifier. This is used to sort and search in a RecordResource list.

@param RecordResource* left
param RecordResource* right
*/
int record_resource_compare(const RecordResource* left, const RecordResource* right)
{
    return strcmp(left->identifier.characters, right->identifier.characters);
}

#ifdef _MSC_VER
#pragma endregion Record
#endif

#ifdef _MSC_VER
#pragma region RecordResourceList
#endif

/**
RecordResourceList holds a dynamic array of RecordResource instances
*/
typedef struct RecordResourceList
{
    RecordResource* resources;
    unsigned long allocated_size;
    unsigned long used_size;
} RecordResourceList;

/**
Creates an instance of RecordResourceList.

@param unsigned long size - size of the array
*/
RecordResourceList record_resource_list_create(const unsigned long size)
{
    RecordResourceList record_resource_list;

    record_resource_list.allocated_size = size;
    record_resource_list.used_size = 0;
    record_resource_list.resources = (RecordResource*)malloc(size * sizeof(RecordResource));

    return record_resource_list;
}

/**
Realloces the dynamic array of RecordResource instances.

@param RecordResourceList* record_resource_list
@param unsigned long size - size of the new array
*/
void record_resource_list_reallocate(RecordResourceList* record_resource_list, const unsigned long size)
{
    record_resource_list->allocated_size = size;
    record_resource_list->resources = (RecordResource*)realloc(record_resource_list->resources, size * sizeof(RecordResource));
}

/**
Adda a RecordResource instance to the array of resources in given RecordResourceList.

@param RecordResourceList* record_resource_list
@param RecordResource resource - resource to be added
*/
void record_resource_list_append(RecordResourceList* record_resource_list, RecordResource resource)
{
    if(record_resource_list->allocated_size <= record_resource_list->used_size)
        record_resource_list_reallocate(record_resource_list, record_resource_list->allocated_size * 2);

    if(record_resource_list->used_size == 0)
    {
        record_resource_list->resources[record_resource_list->used_size++] = resource;
        return;
    }

    int insert_index = 0;
    for (unsigned long i = 0; i < record_resource_list->used_size; i++)
    {
        if(strcmp(resource.identifier.characters, record_resource_list->resources[i].identifier.characters) < 0)
            break;
        else
            insert_index++;
    }
  
    memmove(&record_resource_list->resources[insert_index + 1], &record_resource_list->resources[insert_index], (record_resource_list->used_size - insert_index) * sizeof(RecordResource));
    record_resource_list->resources[insert_index] = resource;
    record_resource_list->used_size++;

}

/**
Delets a record resource from the system.

@param RecordResourceList* record_resource_list
@param String id
*/
short record_resource_list_delete_record(RecordResourceList* record_resource_list, const String id)
{
    RecordResource search = record_resource_create(string_copy(&id));
    RecordResource* searched_record = (RecordResource*)bsearch(&search, record_resource_list->resources, record_resource_list->used_size, sizeof(RecordResource), (int(*)(const void*, const void*))record_resource_compare);
    if(searched_record)
    {
        if(searched_record->number_record && searched_record->number_record->exists)
        {
            searched_record->number_record->exists = FALSE;
            record_resource_dispose(&search);
            return TRUE;
        }
    }

    record_resource_dispose(&search);
    return FALSE;
}

/**
Appends a record to the RecordResourceList system. It finds a resource that matches the phone number and name converted to T9 and adds them there.

@param RecordResourceList* record_resource_list
@param Record* record - record to be added to the system
*/
short record_resource_list_append_record(RecordResourceList* record_resource_list, Record* record)
{
    String converted_name = string_to_T9(&record->user_name);

    short result = TRUE;

    RecordResource search = record_resource_create(string_copy(&record->phone_number));
    RecordResource* number_record = (RecordResource*)bsearch(&search, record_resource_list->resources, record_resource_list->used_size, sizeof(RecordResource), (int(*)(const void*, const void*))record_resource_compare);
    if(!number_record)
    {
        RecordResource add = record_resource_create(string_copy(&record->phone_number));
        record->pointers++;
        add.number_record = record;
        record_resource_list_append(record_resource_list, add);
    }
    else
    {
        if(number_record->number_record)
        {
            if(number_record->number_record->exists)
                result = UPDATED;
                
            number_record->number_record->exists = FALSE;
            number_record->number_record->pointers--;
        }

        record->pointers++;
        number_record->number_record = record;
    }
    record_resource_dispose(&search);
    search = record_resource_create(string_copy(&converted_name));
    RecordResource* name_record = (RecordResource*)bsearch(&search, record_resource_list->resources, record_resource_list->used_size, sizeof(RecordResource), (int(*)(const void*, const void*))record_resource_compare);
    if(!name_record)
    {
        RecordResource add = record_resource_create(string_copy(&converted_name));
        record_list_append(&add.user_name_records, record);
        record_resource_list_append(record_resource_list, add);
    }
    else
    {
        record_list_append(&name_record->user_name_records, record);
    }

    record_resource_dispose(&search);
    string_dispose(&converted_name);
    return result;
}

/**
Frees all resources used by a given RecordResourceList.

@param RecordResourceList* record_resource_list - instance to be disposed
*/
void record_resource_list_dispose(RecordResourceList* record_resource_list)
{
    if(record_resource_list)
        for (unsigned long i = 0; i < record_resource_list->used_size; i++)
             record_resource_dispose(&record_resource_list->resources[i]);

    free(record_resource_list->resources);   
}

/**
Finds a RecordResource from a given RecordResourceList.

@param RecordResourceList* record_resource_list
@param String identifier - number to be found 
*/
RecordResource* record_resource_list_find_by_identifier(RecordResourceList* record_resource_list, String identifier)
{
    RecordResource search = record_resource_create(string_copy(&identifier));
    RecordResource* searched_record = (RecordResource*)bsearch(&search, record_resource_list->resources, record_resource_list->used_size, sizeof(RecordResource), (int(*)(const void*, const void*))record_resource_compare);
    record_resource_dispose(&search);
    return searched_record;
}

#ifdef _MSC_VER
#pragma endregion RecordResourceList
#endif

#ifdef _MSC_VER
#pragma region InputHandling
#endif

/**
Simply reads from stdin untill '\n' is encountered
*/
short void_line()
{
    while (TRUE)
    {
        char c = getchar();
        if(c == '\n')
            return TRUE;
        else if(c == EOF)
            return EOF;
    }
    
}

/**
Reads phone number from stdin. Returns FALSE if something went wrong or END if EOF or line end was read when it was not supposed to.

@param String* user_name - string to be modified
@param char end_char - tells the method what to expect as the last character
*/
short read_phone_number_from_stdin(String* phone_number, char end_char)
{
    if(!phone_number)
        return FALSE;

    char input;
    while (TRUE)
    {
        input = getchar();
        if(input == ' ')
            continue;
        else if(isdigit(input))
        {
            string_append(phone_number, input);
            break;
        }
        else if(input == EOF || input == '\n')
            return END;
        else
            return FALSE;
    }
    
    while (TRUE)
    {
        input = getchar();
        if(isdigit(input))
            string_append(phone_number, input);
        else if(input == end_char)
        {
            return TRUE;
        }
        else if(input == ' ')
            return FALSE;
        else if(input == EOF || input == '\n')
            return END;
        else
            return FALSE;
    }
}

/**
Reads user name from stdin. Returns FALSE if something went wrong or END if EOF or line end was read when it was not supposed to.

@param String* user_name - string to be modified
*/
short read_user_name_from_stdin(String* user_name)
{
    if(!user_name)
    {
        return FALSE;
    }

    char input;
    while (TRUE)
    {
        input = getchar();
        if(input == ' ')
            continue;
        else if(input == '"')
            break;
        else if(input == EOF || input == '\n')
            return END;
        else
            return FALSE;
    }

    short skip = FALSE;
    while (TRUE)
    {
        input = getchar();
        if(input == '\\')
        {
            if(skip)
            {
                string_append(user_name, input);
                skip = FALSE;
            }
            else
                skip = TRUE;            
        }
        else if(input == '"')
        {
            if(skip)
            {
                string_append(user_name, input);
                skip = FALSE;
            }
            else
                break;
        }
        else if(input == EOF || input == '\n')
        {
            return END;
        }
        else
        {
            if(skip)
                skip = FALSE;

            string_append(user_name, input);
        }
    }
    
    return TRUE;
}

#ifdef _MSC_VER
#pragma endregion InputHandling
#endif

#ifdef _MSC_VER
#pragma region Commands
#endif

/**
Handles the add command (+). Reads the phone number and username from stdin and adds a new Record to the system.

@param RecordRecourseList* resources
*/
short handle_add_command(RecordResourceList* resources)
{
    String number = string_create(2);
    String user_name = string_create(2);
    short result = read_phone_number_from_stdin(&number, ' ');
    if(result == FALSE || result == END)
    {
        printf("INVALID COMMAND\n");
        string_dispose(&number);
        string_dispose(&user_name);
        if(result == FALSE)
            return void_line();
        return TRUE;
    }
    result = read_user_name_from_stdin(&user_name);
    if(result == FALSE || result == END)
    {
        printf("INVALID COMMAND\n");
        string_dispose(&number);
        string_dispose(&user_name);
        if(result == FALSE)
            return void_line();
        return TRUE;
    }

    if(getchar() != '\n')
    {
        printf("INVALID COMMAND\n");
        string_dispose(&number);
        string_dispose(&user_name);
        return void_line();
    }

    Record* record = record_create(string_copy(&number), string_copy(&user_name));

    result = record_resource_list_append_record(resources, record);
    if(result == UPDATED)    
        printf("UPDATED\n");
    else
        printf("NEW\n");

    string_dispose(&number);
    string_dispose(&user_name);
    return TRUE;
}

/**
Handles the delete command (-). Reads the phone number from stdin and deletes the corresponding query.

@param RecordRecourseList* resources
*/
short handle_delete_command(RecordResourceList* resources)
{
    String number = string_create(2);
    short result = read_phone_number_from_stdin(&number, '\n');
    if(result == FALSE || result == END)
    {
        printf("INVALID COMMAND\n");
        string_dispose(&number);
        if(result == FALSE)
            return void_line();
        return TRUE;
    }

    result = record_resource_list_delete_record(resources, number);
    if(result)    
        printf("DELETED\n");
    else
        printf("NOT FOUND\n");

    string_dispose(&number);
    return TRUE;
}

/**
Handles the find command (?). Reads the number from stdin and finds all records that correspond with the number.

@param RecordRecourseList* resources
*/
short handle_find_command(RecordResourceList* resources)
{
    String number = string_create(2);
    short result = read_phone_number_from_stdin(&number, '\n');
    if(result == FALSE || result == END)
    {
        printf("INVALID COMMAND\n");
        string_dispose(&number);
        if(result == FALSE)
            return void_line();
        return TRUE;
    }

    RecordResource* resource = record_resource_list_find_by_identifier(resources, number);
    if(!resource)
    {
        printf("NOT FOUND\n");
        string_dispose(&number);
        return TRUE;
    }

    RecordList existing_records = record_list_create(2);
    
    if(resource->number_record && resource->number_record->exists)
        record_list_append(&existing_records, resource->number_record);
    
    for (unsigned long i = 0; i < resource->user_name_records.used_size; i++)
    {
        if(resource->user_name_records.records[i] && resource->user_name_records.records[i]->exists && 
        (resource->number_record != resource->user_name_records.records[i]))
            record_list_append(&existing_records, resource->user_name_records.records[i]);
    }
    
    if(existing_records.used_size == 0)
        printf("NOT FOUND\n");
    else if(existing_records.used_size == 1)
        record_print(existing_records.records[0]);
    else
        printf("AMBIGUOUS (%lu matches)\n", existing_records.used_size);


    string_dispose(&number);
    record_list_dispose(&existing_records);

    return TRUE;
}

#ifdef _MSC_VER
#pragma endregion Commands
#endif

/*
Creates the RecordResourceList to be used by the program and reads command characters from stdin.
*/
int main (void)
{
    RecordResourceList resources = record_resource_list_create(2);

    printf("PBX configuration (+ = set, - = delete, ? = test, EOF = quit):\n");

    while (TRUE)
    {
        char command = getchar();
        if(command == EOF)
        {
            record_resource_list_dispose(&resources);
            return 0;
        }
        else if(command == '\n')
        {
            printf("INVALID COMMAND\n");
            continue;
        }
        char space = getchar();
        if(space != ' ')
        {
            printf("INVALID COMMAND\n");
            if(space == EOF)
            {
                record_resource_list_dispose(&resources);
                return 0;
            }
            else if(space != '\n')
                void_line();
            continue;
        }
        switch (command)
        {
            case '+':
                if(handle_add_command(&resources) == EOF)
                {
                    record_resource_list_dispose(&resources);
                    return 0;
                }
                break;
            
            case '-':
                if(handle_delete_command(&resources) == EOF)
                {
                    record_resource_list_dispose(&resources);
                    return 0;
                }
                break;

            case '?':
                if(handle_find_command(&resources) == EOF)
                {
                    record_resource_list_dispose(&resources);
                    return 0;
                }
                break;

            default:
                printf("INVALID COMMAND\n");
                if(void_line() == EOF)
                {
                    record_resource_list_dispose(&resources);
                    return 0;  
                }
                break;
        }
    }
}