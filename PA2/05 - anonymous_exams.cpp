#ifndef __PROGTEST__
#include <cassert>
#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>
#include <set>
#include <list>
#include <map>
#include <algorithm>
using namespace std;

class CResult
{
public:

    CResult( const string & name, unsigned int   studentID, const string & test, int result ) : m_Name(name), m_StudentID(studentID), m_Test(test), m_Result(result)
    {
    }

    bool operator == ( const CResult& other ) const
    {
        return m_Name == other . m_Name 
        && m_StudentID == other . m_StudentID
        && m_Test == other . m_Test
        && m_Result == other . m_Result;
    }

    string         m_Name;
    unsigned int   m_StudentID;
    string         m_Test;
    int            m_Result;
};
#endif /* __PROGTEST__ */

class StudentCard
{
public:
    StudentCard(const string & cardId)
    {
        CardId = cardId;
    }

    string CardId;
    void* Holder;
};

struct StudentCardComparator
{
    bool operator() (const StudentCard* left, const StudentCard* right) const
    {
        return left->CardId < right->CardId;
    }
};

class Student
{
public:
    Student(const unsigned int id)
    {
        ID = id;
    }

    unsigned int ID;
    string Name;

    set<StudentCard*> Cards;
};

struct StudentComparator
{
    bool operator() (const Student * left, const Student * right) const
    {
        return left->ID < right->ID;
    }
};

struct StudentStringComparator
{
    bool operator() (const Student * left, const string right) const
    {
        return left->Name == right;
    }
};

class StudentGrade
{
public:
    StudentGrade(unsigned int _id, Student * _student, int grade)
    {
        id = _id;
        student = _student;
        Grade = grade;
    }

    unsigned int id;
    Student* student;
    int Grade;

};

class Test
{
public:
    Test(const string name)
    {
        Name = name;
    }

    string Name;

    set<Student*, StudentComparator> RegisteredStudents;
    set<Student*, StudentComparator> AssessedStudents;
    vector<StudentGrade> Grades;
};

struct TestComparator
{
    bool operator() (const Test* left, const Test* right) const
    {
        return left->Name < right->Name;
    }
};

class CExam
{
public:
    static const int SORT_NONE   = 0;
    static const int SORT_ID     = 1;
    static const int SORT_NAME   = 2;
    static const int SORT_RESULT = 3;

    bool Load( istream & cardMap );
    bool Register( const string & cardID, const string & test_name );
    bool Assess( unsigned int   studentID, const string & test_name, int result );
    list<CResult> ListTest( const string & testName, int sortBy ) const;
    set<unsigned int> ListMissing ( const string & testName ) const;

    set<Test*, TestComparator> Tests;
    set<Student*, StudentComparator> Students;
    set<StudentCard*, StudentCardComparator> Cards;

private:
// todo
};

bool isNumber(const string & to_check)
{
    return !to_check.empty() && std::find_if(to_check.begin(), to_check.end(), [](unsigned char c) { return !std::isdigit(c); }) == to_check.end();
}

bool CExam::Load( istream & cardMap )
{
    string line;
    vector<Student*> tempStudents;
    vector<StudentCard*> tempCards;
    while (getline(cardMap, line))
    {
        string delimeter = ":";
        if(line.empty())
        {
            for(auto student : tempStudents)
                delete(student);

            for(auto card : tempCards)
                delete(card);

            return false;
        }
        
        size_t ind = line.find(delimeter);
        string raw_id = line.substr(0, ind);
        line.erase(0, ind + delimeter.length());

        if(!isNumber(raw_id))
        {
            for(auto student : tempStudents)
                delete(student);

            for(auto card : tempCards)
                delete(card);

            return false;
        }

        Student* new_student = new Student(stoul(raw_id));
        auto found = Students.find(new_student);
        if(found != Students.end())
        {
            for(auto student : tempStudents)
                delete(student);

            for(auto card : tempCards)
                delete(card);

            delete(new_student);
            return false;
        }

        ind = line.find(delimeter);
        string name = line.substr(0, ind);
        line.erase(0, ind + delimeter.length());
        new_student->Name = name;

        delimeter = ",";
        while (!line.empty())
        {
            ind = line.find(delimeter);
            string card = line.substr(0, ind);
            line.erase(0, card.length() + delimeter.length());
            if(card.empty())
            {
                for(auto student : tempStudents)
                    delete(student);

                for(auto card : tempCards)
                    delete(card);

                delete(new_student);
                return false;
            }

            size_t start = card.find_first_not_of(' ');
            size_t end = card.find_last_not_of(' ');
            card = card.substr(start, (end - start + 1));

            StudentCard* new_card = new StudentCard(card);
            auto card_found = Cards.find(new_card);
            if(card_found != Cards.end())
            {
                for(auto student : tempStudents)
                    delete(student);

                for(auto card : tempCards)
                    delete(card);

                delete(new_student);
                delete(new_card);
                return false;
            }

            new_student->Cards.insert(new_card);
            new_card->Holder = new_student;
            tempCards.push_back(new_card);
        }
        
        tempStudents.push_back(new_student);
    }
    
    for(auto student : tempStudents)
        Students.insert(student);

    for(auto card : tempCards)
        Cards.insert(card);

    return true;
}

bool CExam::Register( const string & cardID, const string & test_name )
{
    StudentCard* card = new StudentCard(cardID);
    auto card_found = Cards.find(card);
    delete(card);
    if(card_found == Cards.end())
        return false;

    auto card_index = distance(Cards.begin(), card_found);
    card = *next(Cards.begin(), card_index);

    Student* student = (Student*)card->Holder;

    Test* test = new Test(test_name);
    auto test_found = Tests.find(test);
    if(test_found == Tests.end())
        Tests.insert(test);
    else
    {
        auto test_index = distance(Tests.begin(), test_found);
        test = *next(Tests.begin(), test_index);
    }

    auto registered_found = test->RegisteredStudents.find(student);
    if(registered_found != test->RegisteredStudents.end())
        return false;

    test->RegisteredStudents.insert(student);

    return true;
}

bool CExam::Assess( unsigned int studentID, const string & test_name, int result )
{
    Student* student = new Student(studentID);
    auto student_found = Students.find(student);
    delete(student);
    if(student_found == Students.end())
    {
        return false;
    }

    auto student_index = distance(Students.begin(), student_found);
    student = *next(Students.begin(), student_index);

    Test* test = new Test(test_name);
    auto test_found = Tests.find(test);
    if(test_found == Tests.end())
    {
        delete(test);
        return false;
    }

    auto test_index = distance(Tests.begin(), test_found);
    test = *next(Tests.begin(), test_index);

    auto registered_found = test->RegisteredStudents.find(student);
    auto assessed_found = test->AssessedStudents.find(student);
    if(registered_found == test->RegisteredStudents.end() ||
        assessed_found != test->AssessedStudents.end())
        return false;
    
    test->AssessedStudents.insert(student);
    StudentGrade new_grade = StudentGrade((unsigned int)test->Grades.size() ,student, result);
    test->Grades.push_back(new_grade);
    return true;
}

struct GradeIdComparator
{
    bool operator() (const StudentGrade & left, const StudentGrade & right) const
    {
        return ((Student*)left.student)->ID < ((Student*)right.student)->ID;
    }
};

struct GradeNameComparator
{
    bool operator() (const StudentGrade & left, const StudentGrade & right) const
    {
        Student* left_s = (Student*)left.student;
        Student* right_s = (Student*)right.student;

        if(left_s->Name == right_s->Name)
            return left.id < right.id;
        
        return left_s->Name < right_s->Name;
    }
};

struct GradeResultComparator
{
    bool operator() (const StudentGrade & left, const StudentGrade & right) const
    {
        if(left.Grade == right.Grade)
            return left.id < right.id;

        return left.Grade > right.Grade;
    }
};

list<CResult> CExam::ListTest( const string & testName, int sortBy ) const
{
    list<CResult> results;

    Test* test = new Test(testName);
    auto test_found = Tests.find(test);
    if(test_found == Tests.end())
    {
        delete(test);
        return results;
    }

    auto test_index = distance(Tests.begin(), test_found);
    test = *next(Tests.begin(), test_index);

    if(sortBy == SORT_NONE)
    {
        for(auto grade : test->Grades)
        {
            Student* s = (Student*)grade.student;
            CResult new_result = CResult(s->Name, s->ID, testName, grade.Grade);
            results.push_back(new_result);
        }
    }
    else if(sortBy == SORT_ID)
    {
        set<StudentGrade, GradeIdComparator> grades;
        for(auto grade : test->Grades)
        {
            grades.insert(grade);
        }
        auto iterator = grades.begin();
        while (iterator != grades.end())
        {
            StudentGrade grade = *iterator;
            Student* s = (Student*)grade.student;
            CResult new_result = CResult(s->Name, s->ID, testName, grade.Grade);
            results.push_back(new_result);
            iterator++;
        }
        
    }
    else if(sortBy == SORT_NAME)
    {
        set<StudentGrade, GradeNameComparator> grades;
        for(auto grade : test->Grades)
        {
            grades.insert(grade);
        }
        auto iterator = grades.begin();
        while (iterator != grades.end())
        {
            StudentGrade grade = *iterator;
            Student* s = (Student*)grade.student;
            CResult new_result = CResult(s->Name, s->ID, testName, grade.Grade);
            results.push_back(new_result);
            iterator++;
        }
    }
    else
    {
        set<StudentGrade, GradeResultComparator> grades;
        for(auto grade : test->Grades)
        {
            grades.insert(grade);
        }
        auto iterator = grades.begin();
        while (iterator != grades.end())
        {
            StudentGrade grade = *iterator;
            Student* s = (Student*)grade.student;
            CResult new_result = CResult(s->Name, s->ID, testName, grade.Grade);
            results.push_back(new_result);
            iterator++;
        }
    }

    return results;
}

set<unsigned int> CExam::ListMissing ( const string & testName ) const
{
    set<unsigned int> missing;

    Test* test = new Test(testName);
    auto test_found = Tests.find(test);
    if(test_found == Tests.end())
    {
        delete(test);
        return missing;
    }

    auto test_index = distance(Tests.begin(), test_found);
    test = *next(Tests.begin(), test_index);

    auto iterator = test->RegisteredStudents.begin();
    while (iterator != test->RegisteredStudents.end())
    {
        Student* student = *iterator;
        auto student_found = test->AssessedStudents.find(student);
        if(student_found == test->AssessedStudents.end())
            missing.insert(student->ID);

        iterator++;
    }

    return missing;
}

#ifndef __PROGTEST__
int main ( void )
{
    istringstream iss;
    CExam         m;
    iss . clear ();
    iss . str ( "123456:Smith John:er34252456hjsd2451451, 1234151asdfe5123416, asdjklfhq3458235\n"
    "654321:Nowak Jane: 62wtsergtsdfg34\n"
    "456789:Nowak Jane: okjer834d34\n"
    "987:West Peter Thomas:sdswertcvsgncse\n" );
    assert ( m . Load ( iss ) );

    assert ( m . Register ( "62wtsergtsdfg34", "PA2 - #1" ) );
    assert ( m . Register ( "62wtsergtsdfg34", "PA2 - #2" ) );
    assert ( m . Register ( "er34252456hjsd2451451", "PA2 - #1" ) );
    assert ( m . Register ( "er34252456hjsd2451451", "PA2 - #3" ) );
    assert ( m . Register ( "sdswertcvsgncse", "PA2 - #1" ) );
    assert ( !m . Register ( "1234151asdfe5123416", "PA2 - #1" ) );
    assert ( !m . Register ( "aaaaaaaaaaaa", "PA2 - #1" ) );
    assert ( m . Assess ( 123456, "PA2 - #1", 50 ) );
    assert ( m . Assess ( 654321, "PA2 - #1", 30 ) );
    assert ( m . Assess ( 654321, "PA2 - #2", 40 ) );
    assert ( m . Assess ( 987, "PA2 - #1", 100 ) );
    assert ( !m . Assess ( 654321, "PA2 - #1", 35 ) );
    assert ( !m . Assess ( 654321, "PA2 - #3", 35 ) );
    assert ( !m . Assess ( 999999, "PA2 - #1", 35 ) );
    assert ( m . ListTest ( "PA2 - #1", CExam::SORT_RESULT ) == (list<CResult>
    {
    CResult ( "West Peter Thomas", 987, "PA2 - #1", 100 ),
    CResult ( "Smith John", 123456, "PA2 - #1", 50 ),
    CResult ( "Nowak Jane", 654321, "PA2 - #1", 30 )
    } ) );
    assert ( m . ListTest ( "PA2 - #1", CExam::SORT_NAME ) == (list<CResult>
    {
    CResult ( "Nowak Jane", 654321, "PA2 - #1", 30 ),
    CResult ( "Smith John", 123456, "PA2 - #1", 50 ),
    CResult ( "West Peter Thomas", 987, "PA2 - #1", 100 )
    } ) );
    assert ( m . ListTest ( "PA2 - #1", CExam::SORT_NONE ) == (list<CResult>
    {
    CResult ( "Smith John", 123456, "PA2 - #1", 50 ),
    CResult ( "Nowak Jane", 654321, "PA2 - #1", 30 ),
    CResult ( "West Peter Thomas", 987, "PA2 - #1", 100 )
    } ) );
    assert ( m . ListTest ( "PA2 - #1", CExam::SORT_ID ) == (list<CResult>
    {
    CResult ( "West Peter Thomas", 987, "PA2 - #1", 100 ),
    CResult ( "Smith John", 123456, "PA2 - #1", 50 ),
    CResult ( "Nowak Jane", 654321, "PA2 - #1", 30 )
    } ) );
    assert ( m . ListMissing ( "PA2 - #3" ) == (set<unsigned int>{ 123456 }) );
    iss . clear ();
    iss . str ( "888:Watson Joe:25234sdfgwer52, 234523uio, asdf234235we, 234234234\n" );
    assert ( m . Load ( iss ) );

    assert ( m . Register ( "234523uio", "PA2 - #1" ) );
    assert ( m . Assess ( 888, "PA2 - #1", 75 ) );
    iss . clear ();
    iss . str ( "555:Gates Bill:ui2345234sdf\n"
    "888:Watson Joe:2345234634\n" );
    assert ( !m . Load ( iss ) );

    assert ( !m . Register ( "ui2345234sdf", "PA2 - #1" ) );
    iss . clear ();
    iss . str ( "555:Gates Bill:ui2345234sdf\n"
    "666:Watson Thomas:okjer834d34\n" );
    assert ( !m . Load ( iss ) );

    assert ( !m . Register ( "ui2345234sdf", "PA2 - #3" ) );
    iss . clear ();
    iss . str ( "555:Gates Bill:ui2345234sdf\n"
    "666:Watson Thomas:jer834d3sdf4\n" );
    assert ( m . Load ( iss ) );

    assert ( m . Register ( "ui2345234sdf", "PA2 - #3" ) );
    assert ( m . ListMissing ( "PA2 - #3" ) == (set<unsigned int>{ 555, 123456 }) );

    return 0;
}
#endif /* __PROGTEST__ */
