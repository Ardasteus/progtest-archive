#ifndef __PROGTEST__
#include <iostream>
#include <iomanip>
#include <sstream>
#include <cstdint>
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <deque>
#include <algorithm>
#include <memory>
#include <functional>
#include <stdexcept>
using namespace std;
#endif /* __PROGTEST__ */

class CPerson
{
protected:
    int id;
    string name;
    vector<shared_ptr<CPerson>> descendants;
    shared_ptr<CPerson> firstParent;
    shared_ptr<CPerson> secondParent;
    function<void(string, int)> callbackFunc;

    virtual void PrintPerson(ostream& os) const
    {
        os << id;
    }

public:
    CPerson(const int& _id, const string& _name)
    {
        id = _id;
        name = _name;
        firstParent = nullptr;
        secondParent = nullptr;
    }

    virtual ~CPerson() 
    {
        descendants.clear();
        firstParent = nullptr;
        secondParent = nullptr;
    }

    void Release()
    {
        descendants.clear();
        firstParent = nullptr;
        secondParent = nullptr;
    }

    void SetCallback(function<void(string, int)> callback)
    {
        callbackFunc = callback;
    }

    int GetID() const
    {
        return id;
    }

    string GetName() const
    {
        return name;
    }

    size_t CountDescendants() const;

    void AddDescendant(shared_ptr<CPerson> descendant)
    {
        descendants.push_back(descendant);
    }

    vector<shared_ptr<CPerson>> GetRelatives() const
    {
        vector<shared_ptr<CPerson>> relatives;

        for(auto descendant : descendants)
            relatives.push_back(descendant);
        
        if(firstParent != nullptr)
            relatives.push_back(firstParent);
        
        if(secondParent != nullptr)
            relatives.push_back(secondParent);

        return relatives;
    }

    void SetParent(shared_ptr<CPerson> parent)
    {
        if(firstParent == nullptr)
            firstParent = parent;

        else if(secondParent == nullptr)
            secondParent = parent;
    }

    virtual bool ComparePrefix(const string& prefix) const
    {
        if(name.size() < prefix.size())
            return false;
        return name.compare(0, prefix.size(), prefix) == 0;
    }

    friend std::ostream & operator<<( std::ostream& os, const CPerson& person )
    {
        person.PrintPerson(os);
        return os;
    }
};

struct CPersonIdComparator
{
    bool operator() (const shared_ptr<CPerson> left, const shared_ptr<CPerson> right ) const
    {
        return left->GetID() < right->GetID();
    }
};

class CPersonRelativeChain
{
public:
    shared_ptr<CPerson> person;
    std::list<shared_ptr<CPerson>> trail;

    CPersonRelativeChain(shared_ptr<CPerson> _person)
    {
        person = _person;
    }
};

class CPersonBreadtSearch
{
private:
    std::queue<shared_ptr<CPersonRelativeChain>> queue;
    std::set<shared_ptr<CPerson>, CPersonIdComparator> people;

public:
    CPersonBreadtSearch() 
    {
    
    }
    ~CPersonBreadtSearch() 
    {
        people.clear();
    }

    void push(shared_ptr<CPersonRelativeChain> chain)
    {
        auto found = people.find(chain->person);
        if(found != people.end())
            return;

        people.insert(chain->person);
        queue.push(chain);
    }

    shared_ptr<CPersonRelativeChain> pop()
    {
        shared_ptr<CPersonRelativeChain> person = queue.front();
        queue.pop();
        return person;
    }

    bool empty() const
    {
        return queue.empty();
    }

    size_t GetUniqueCount()
    {
        return people.size();
    }

};

class CPersonDescendants
{
private:
    std::queue<shared_ptr<CPerson>> queue;
    std::set<shared_ptr<CPerson>, CPersonIdComparator> people;

public:
    CPersonDescendants() 
    {
    
    }
    ~CPersonDescendants() 
    {
        people.clear();
    }

    void push(shared_ptr<CPerson> person)
    {
        auto found = people.find(person);
        if(found != people.end())
            return;

        people.insert(person);
        queue.push(person);
    }

    shared_ptr<CPerson> pop()
    {
        shared_ptr<CPerson> person = queue.front();
        queue.pop();
        return person;
    }

    bool empty() const
    {
        return queue.empty();
    }

    size_t GetUniqueCount()
    {
        return people.size();
    }
};

size_t CPerson::CountDescendants() const
{
    CPersonDescendants queue;

    for(auto desc : descendants)
        queue.push(desc);

    while(!queue.empty())
    {
        shared_ptr<CPerson> person = queue.pop();
        for(auto desc : person->descendants)
            queue.push(desc);
    }
    return queue.GetUniqueCount();
}

class CMan : public CPerson
{
public:
    CMan(const int& _id, const string& _name) 
    : CPerson(_id, _name)
    {

    }
protected:
    void PrintPerson(ostream& os) const override
    {
        os << id << ": " << name << " (man)";
    }
};

class CWoman : public CPerson
{
public:
    CWoman(const int& _id, const string& _name) 
    : CPerson(_id, _name)
    {
        maidenName = _name;
        hasWed = false;
    }

    bool Wedding(const string& newName)
    {
        if(hasWed)
            return false;

        name = newName;
        hasWed = true;
        callbackFunc(newName, id);
        return true;
    }

    bool ComparePrefix(const string& prefix) const override
    {
        return ComparePrefixName(prefix) || ComparePrefixWed(prefix); 
    }

protected:
    string maidenName;
    bool hasWed;

    bool ComparePrefixName(const string& prefix) const
    {
        if(name.size() < prefix.size())
            return false;
        return name.compare(0, prefix.size(), prefix) == 0;
    }

    bool ComparePrefixWed(const string& prefix) const
    {
        if(maidenName.size() < prefix.size())
            return false;
        return maidenName.compare(0, prefix.size(), prefix) == 0;
    }

    //12: Smith Sue [born: Peterson Sue] (woman)
    void PrintPerson(ostream& os) const override
    {
        if(!hasWed)
            os << id << ": " << name << " (woman)";
        else
            os << id << ": " << name << " [born: " << maidenName << "] (woman)";
    }
};

class CPersonNamePair
{
public:
    string name;
    shared_ptr<CPerson> person;

    CPersonNamePair(const string& _name, shared_ptr<CPerson> _person)
    {
        name = _name;
        person = _person;
    }
};

struct CPersonNameComparator
{
    bool operator() (const shared_ptr<CPersonNamePair> left, const shared_ptr<CPersonNamePair> right ) const
    {
        if(left->name == right->name && left->person != nullptr && right->person != nullptr)
            return left->person->GetID() < right->person->GetID();

        return left->name < right->name;
    }
};

class CRegister
{
public:
    CRegister()
    {

    }

    ~CRegister()
    {
        for(auto person : PeopleById)
            person->Release();

        PeopleById.clear();
    }

    bool Add(shared_ptr<CPerson> person, shared_ptr<CPerson> father, const shared_ptr<CPerson> mother)
    {
        auto person_found = PeopleById.find(person);
        if(person_found != PeopleById.end())
            return false;

        if(father != nullptr)
        {
            person->SetParent(father);
            father->AddDescendant(person);
        }
        if(mother != nullptr)
        {
            person->SetParent(mother);
            mother->AddDescendant(person);
        }

        person->SetCallback([&](string name, int id) -> void
        {
            shared_ptr<CPerson> to_update = FindByID(id);
            shared_ptr<CPersonNamePair> pair = std::make_shared<CPersonNamePair>(name, to_update);
            PeopleByName.insert(pair);
        });
        PeopleById.insert(person);
        shared_ptr<CPersonNamePair> pair = std::make_shared<CPersonNamePair>(person->GetName(), person);
        PeopleByName.insert(pair);
        return true;
    }

    shared_ptr<CPerson> FindByID(const int& id) const
    {
        shared_ptr<CPerson> person = make_shared<CPerson>(id, "Find me");
        auto person_found = PeopleById.find(person);
        if(person_found != PeopleById.end())
            return *person_found;
        
        return nullptr;
    }

    vector<shared_ptr<CPerson>> FindByName(const string& prefix) const
    {
        set<shared_ptr<CPerson>, CPersonIdComparator> temp;
        shared_ptr<CPersonNamePair> pair = make_shared<CPersonNamePair>(prefix, nullptr);
        auto index = PeopleByName.lower_bound(pair);
        while (true)
        {
            if(index == PeopleByName.end())
                break;
            shared_ptr<CPersonNamePair> checkPair = *index++;
            shared_ptr<CPerson> person = checkPair->person;
            bool hasPrefix = person->ComparePrefix(prefix);
            if(hasPrefix)
                temp.insert(person);
            else
                break;

        }

        return vector<shared_ptr<CPerson>>(temp.begin(), temp.end());
    }

    list<shared_ptr<CPerson>> FindRelatives(const int& id1, const int& id2 ) const
    {
        list<shared_ptr<CPerson>> people;

        shared_ptr<CPerson> person_start = make_shared<CPerson>(id1, "Find me");
        auto person_start_found = PeopleById.find(person_start);
        if(person_start_found == PeopleById.end())
            throw invalid_argument("Person does not exist (id1)");

        person_start = *person_start_found;

        if(id1 == id2)
        {
            people.push_back(person_start);
            return people;
        }

        shared_ptr<CPerson> person_end = make_shared<CPerson>(id2, "Find me");
        auto person_end_found = PeopleById.find(person_end);
        if(person_end_found == PeopleById.end())
            throw invalid_argument("Person does not exist (id2)");
        
        person_end = *person_end_found;

        CPersonBreadtSearch queue;
        shared_ptr<CPersonRelativeChain> chain = std::make_shared<CPersonRelativeChain>(person_start);
        chain->trail.push_back(person_start);
        queue.push(chain);

        while(!queue.empty())
        {
            shared_ptr<CPersonRelativeChain> person = queue.pop();
            if(person->person->GetID() == person_end->GetID())
            {
                people = person->trail;
                break;
            }
            
            for(auto relative : person->person->GetRelatives())
            {
                shared_ptr<CPersonRelativeChain> next = std::make_shared<CPersonRelativeChain>(relative);
                for(auto item : person->trail)
                {
                    next->trail.push_back(item);
                }
                next->trail.push_back(next->person);
                queue.push(next);
            }
        }

        return people;
    }

private:
    set<shared_ptr<CPerson>, CPersonIdComparator> PeopleById;
    set<shared_ptr<CPersonNamePair>, CPersonNameComparator> PeopleByName;
};

#ifndef __PROGTEST__
template <typename T_>
static bool        vectorMatch ( const vector<T_>     & res,
const vector<string> & ref )
{
vector<string> tmp;
for ( const auto & x : res )
{
ostringstream oss;
oss << *x;
tmp . push_back ( oss . str () );
}
return tmp == ref;
}

template <typename T_>
static bool        listMatch ( const list<T_>     & res,
const list<string> & ref )
{
list<string> tmp;
for ( const auto & x : res )
{
ostringstream oss;
oss << *x;
tmp . push_back ( oss . str () );
}
return tmp == ref;
}

int main ( void )
{
ostringstream oss;

    shared_ptr<CPerson> test = make_shared<CMan> ( 1, "Peterson George");
    oss << *test;

CRegister r;
assert ( r . Add ( make_shared<CMan> ( 1, "Peterson George" ),
nullptr, nullptr ) == true );
assert ( r . Add ( make_shared<CMan> ( 2, "Watson Paul" ),
nullptr, nullptr ) == true );
assert ( r . Add ( make_shared<CMan> ( 10, "Smith Samuel" ),
nullptr, nullptr ) == true );
assert ( r . Add ( make_shared<CWoman> ( 11, "Peterson Jane" ),
r . FindByID ( 1 ), nullptr ) == true );
assert ( r . Add ( make_shared<CWoman> ( 12, "Peterson Sue" ),
r . FindByID ( 1 ), nullptr ) == true );
assert ( r . Add ( make_shared<CMan> ( 13, "Pershing John" ),
nullptr, nullptr ) == true );
size_t t = r.FindByName("Pershing John").size();
assert ( dynamic_cast<CWoman &> ( *r . FindByID ( 12 ) ) . Wedding ( "Smith Sue" ) == true );
assert ( dynamic_cast<CWoman &> ( *r . FindByID ( 12 ) ) . Wedding ( "Watson Sue" ) == false );
assert ( r . Add ( make_shared<CMan> ( 100, "Smith John" ),
r . FindByID ( 10 ), r . FindByID ( 11 ) ) == true );
assert ( r . Add ( make_shared<CMan> ( 101, "Smith Roger" ),
r . FindByID ( 10 ), r . FindByID ( 11 ) ) == true );
assert ( r . Add ( make_shared<CMan> ( 102, "Smith Daniel" ),
r . FindByID ( 10 ), r . FindByID ( 11 ) ) == true );
assert ( r . Add ( make_shared<CWoman> ( 103, "Smith Eve" ),
r . FindByID ( 10 ), r . FindByID ( 11 ) ) == true );
assert ( r . Add ( make_shared<CWoman> ( 103, "Smith Jane" ),
r . FindByID ( 10 ), r . FindByID ( 11 ) ) == false );
assert ( r . Add ( make_shared<CMan> ( 150, "Pershing Joe" ),
r . FindByID ( 13 ), r . FindByID ( 12 ) ) == true );
assert ( r . Add ( make_shared<CMan> ( 200, "Pershing Peter" ),
r . FindByID ( 150 ), r . FindByID ( 103 ) ) == true );
assert ( r . Add ( make_shared<CWoman> ( 201, "Pershing Julia" ),
r . FindByID ( 150 ), r . FindByID ( 103 ) ) == true );
assert ( r . Add ( make_shared<CWoman> ( 202, "Pershing Anne" ),
r . FindByID ( 150 ), r . FindByID ( 103 ) ) == true );

assert ( vectorMatch ( r . FindByName ( "Peterson" ), vector<string>
{
"1: Peterson George (man)",
"11: Peterson Jane (woman)",
"12: Smith Sue [born: Peterson Sue] (woman)"
} ) );
assert ( vectorMatch ( r . FindByName ( "Pe" ), vector<string>
{
"1: Peterson George (man)",
"11: Peterson Jane (woman)",
"12: Smith Sue [born: Peterson Sue] (woman)",
"13: Pershing John (man)",
"150: Pershing Joe (man)",
"200: Pershing Peter (man)",
"201: Pershing Julia (woman)",
"202: Pershing Anne (woman)"
} ) );
assert ( vectorMatch ( r . FindByName ( "Smith" ), vector<string>
{
"10: Smith Samuel (man)",
"12: Smith Sue [born: Peterson Sue] (woman)",
"100: Smith John (man)",
"101: Smith Roger (man)",
"102: Smith Daniel (man)",
"103: Smith Eve (woman)"
} ) );
assert ( r . FindByID ( 1 ) -> GetID () == 1 );
oss . str ( "" );
oss << * r . FindByID ( 1 );
assert ( oss . str () == "1: Peterson George (man)" );
assert ( r . FindByID ( 1 ) -> CountDescendants () == 10 );
assert ( r . FindByID ( 2 ) -> GetID () == 2 );
oss . str ( "" );
oss << * r . FindByID ( 2 );
assert ( oss . str () == "2: Watson Paul (man)" );
assert ( r . FindByID ( 2 ) -> CountDescendants () == 0 );
assert ( listMatch ( r . FindRelatives ( 100, 1 ), list<string>
{
"100: Smith John (man)",
"11: Peterson Jane (woman)",
"1: Peterson George (man)"
} ) );
assert ( listMatch ( r . FindRelatives ( 100, 13 ), list<string>
{
"100: Smith John (man)",
"10: Smith Samuel (man)",
"103: Smith Eve (woman)",
"200: Pershing Peter (man)",
"150: Pershing Joe (man)",
"13: Pershing John (man)"
} ) );
assert ( listMatch ( r . FindRelatives ( 100, 2 ), list<string>
{

} ) );
assert ( listMatch ( r . FindRelatives ( 100, 100 ), list<string>
{
"100: Smith John (man)"
} ) );
try
{
r . FindRelatives ( 100, 3 );
assert ( "Missing an exception" == nullptr );
}
catch ( const invalid_argument & e )
{
}
catch ( ... )
{
assert ( "An unexpected exception thrown" );
}
return 0;
}
#endif /* __PROGTEST__ */
